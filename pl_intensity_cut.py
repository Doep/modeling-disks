#!/user/homedir/demarsd/.nix-profile/bin/python

"""

#######################################
##### This is pl_intensity_cut.py #####
#######################################

This routine will plot the intensity in the integrated channel map along a cut at a given position, either x=cut or y=cut.

-> Note that you can choose to integrate between vmin and vmax if you wish to exclude some rays, by giving -vmin and -vmax parameters

-> It is possible to plot a cut from multiple cubes on the same graph, simply by giving multiple cube names after the -f parameter

##################################

"""


import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-f', '--filename', nargs = '+', help = 'fits cubes to plot the intensity cuts from', type = str, required = True)
group = parser.add_mutually_exclusive_group(required=True)
group.add_argument('-x', '--xaxis', help = 'cut at x = given value', type = float)
group.add_argument('-y', '--yaxis', help = 'cut at y = given value', type = float)
parser.add_argument('-s', '--savename', help = 'savename for the graph', type = str)
parser.add_argument('-vmin', help = 'minimum speed for the integration of the channel map', type =float)
parser.add_argument('-vmax', help = 'maximum speed for the intergation of the channel map', type =float)
parser.add_argument('-xmin', help = 'x minimum value on the graph; if you set xmin, you must also set xmax', type =float)
parser.add_argument('-xmax', help = 'x maximum value on the graph; if you set xmax, you must also set xmin', type =float)
parser.add_argument('-ymin', help = 'y minimum value on the graph; if you set ymin, you must also set ymax', type =float)
parser.add_argument('-ymax', help = 'y maximum value on the graph; if you set ymax, you must also set ymin', type =float)
parser.add_argument('-n', '--normalize', help = 'choose to normalize or not the intensities to the maximum value. default is True', action = 'store_false')
parser.add_argument('-fs', '--figsize', nargs = 2, default = (10,10), help = 'size of the figure, pass as : -fs xsize ysize', type = float)
parser.add_argument('-font', nargs = '?', default = 13, help = 'font size', type = int)
parser.add_argument('-sh', '--show', help = 'show the spectrum after execution', action = 'store_false')

args = parser.parse_args()


filename = args.filename
xaxis = args.xaxis
yaxis = args.yaxis
vmin = args.vmin
vmax = args.vmax
xmin = args.xmin
xmax = args.xmax
ymin = args.ymin
ymax = args.ymax
normalize = args.normalize
savename = args.savename
figsize = args.figsize
font = args.font
show = args.show


from routines import pl_intensity
import matplotlib.pyplot as plt


if xaxis is None:
    axis = 'y'
    cut = yaxis
else:
    axis = 'x'
    cut = xaxis

plt.figure(figsize = figsize)
pl_intensity(filename, axis = axis, cut = cut, savename = savename, normalize = normalize, show = show, vmin = vmin, vmax = vmax)
