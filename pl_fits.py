#!/user/homedir/demarsd/.nix-profile/bin/python

"""

##############################
##### This is pl_fits.py #####
##############################

This routine will plot any 2D fits file, such as Temperature.fits, gas_density.fits, ... 

-> You can use this routine to plot a fits file and add contours from another fits file

##################################

"""


import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-f', '--filename', help = 'fits file to plot', type = str, required = True)
parser.add_argument('-d', '--default', help = 'default parameters file', type = str, required = True)
parser.add_argument('-s', '--savename', nargs ='?', default = 'fits.png', help = 'savename for the graph', type = str)
parser.add_argument('-cmin','--colormin', help = 'maximum abundance value on the graph color bar', type = float)
parser.add_argument('-cmax','--colormax', help = 'maximum abundance value on the graph color bar', type = float)
parser.add_argument('-xmin', help = 'x minimum value on the graph; if you set xmin, you must also set xmax', type =float)
parser.add_argument('-xmax', help = 'x maximum value on the graph; if you set xmax, you must also set xmin', type =float)
parser.add_argument('-ymin', help = 'y minimum value on the graph; if you set ymin, you must also set ymax', type =float)
parser.add_argument('-ymax', help = 'y maximum value on the graph; if you set ymax, you must also set ymin', type =float)
parser.add_argument('-g', '--graph_labels', help = 'either True or False, to force the placing or not of contours and corresponding labels', type = bool)
parser.add_argument('-x', '--xscale', help = 'either linear or log', type = str)
parser.add_argument('-y', '--yscale', help = 'either linear, log or flaring. flaring will draw y = z/R', type = str)
parser.add_argument('-c', '--colormap', help = 'name of the colormap', type = str)
parser.add_argument('-t', '--title', help = 'title of the graph', type = str)
parser.add_argument('-of', '--other_filename', help = 'if set, will draw another fits contours on top of the graph, from given fits file', type = str)
parser.add_argument('-oc', '--other_contours', nargs='+', help = 'list of contours values to draw for the other fits')

args = parser.parse_args()


filename = args.filename
default = args.default
savename = args.savename
cmin = args.colormin
cmax = args.colormax
xmin = args.xmin
xmax = args.xmax
ymin = args.ymin
ymax = args.ymax
labels = args.graph_labels
xscale = args.xscale
yscale = args.yscale
cmap = args.colormap
title = args.title
other_filename = args.other_filename
other_contours = args.other_contours


if other_filename is not None:
    draw_other_contours = True
else:
    draw_other_contours = False


from routines import pl_fits,split_pathnm
import os


splited = split_pathnm(filename)

cwd = os.getcwd()
default = split_pathnm(default)[2]
savename = splited[0] + '/' + savename
grid_file = splited[0] + '/' + 'grid.fits'


pl_fits(savename = savename,
        filename = filename,
        draw_other_contours = draw_other_contours,
        other_contours = other_contours,
        other_filename = other_filename,
        default_filename = default,
        xmin = xmin,
        xmax = xmax,
        ymin = ymin,
        ymax = ymax,
        colormin = cmin,
        colormax = cmax,
        xscale = xscale,
        yscale = yscale,
        draw_contours = labels,
        manual_labels = labels,
        colormap = cmap,
        grid_file = grid_file,
        title = title)

