#!/user/homedir/demarsd/.nix-profile/bin/python

"""

####################################
##### This is pl_parameters.py #####
####################################

This routine will plot multiple the surface density, nwumber density and scale height from parameters given in default filename with -d

-> Note that -s --sigma_zero parameter is mandatory to be able to draw the surface density

-> The graphs are made with 500 points between 1au and r_out, for both R and z. r_out is read from default.para file given with -d

##################################

"""


import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('-d', '--default', help='default filename containing parameters', type = str, required = True)
parser.add_argument('-s', '--sigma_zero', help = 'sigma0 value in the surface density', type = float, required = True)
parser.add_argument('-o', '--output_dir', help = 'output directory for output files. if none given, they will be saved in the default file directory', type = str)
parser.add_argument('-ymin', help = 'y minimum value on the graph for the H2 density; if you set ymin, you must also set ymax', type =float)
parser.add_argument('-ymax', help = 'y maximum value on the graph for the H2 density; if you set ymax, you must also set ymin', type =float)

args = parser.parse_args()


default =  args.default
sigma0 = args.sigma_zero
output_dir = args.output_dir
ymin = args.ymin
ymax = args.ymax


from routines import *
import os
import numpy as np
import matplotlib.pyplot as plt


M_sun = M_sun_cgs
au = au_cgs
uma = uma_cgs


para = read_default_parameters(filename = default)
splited = split_pathnm(default)
dir_name = splited[0]

if output_dir is None:
    output_dir = dir_name

os.chdir(dir_name)

disk_type = para['disk_type']
p1 = para['p1']
p2 = para['p2']
h0 = para['h0']
h = para['h']
r_out = para['r_out']
rc = para['rc']
r0 = para['r0']


r_list = np.linspace(1,r_out, 500)
z_list = np.linspace(1,r_out, 500)


def sigma2(r,sigma0 = sigma0, r0 = r0, p1 = p1, p2 = p2, rc = rc):
    val = sigma0*(r/r0)**(p1) * np.exp(-(r/rc)**(2+p2))
    return val

def sigma1(r,sigma0 = sigma0, r0 = r0, p1 = p1):
    val = sigma0*(r/r0)**(p1)
    return val

def scale_height(r, r0 = r0, h = h, h0 = h0):
    val = h0*(r/r0)**(h)
    return val

def density(sig,sheight,z): #,sigma0 = sigma0,r0=r0,p1=p1,p2=p2,rc=rc,h=h,h0=h0):
    val = sig/(sheight * au_cgs *np.sqrt(np.pi) * np.sqrt(2)) * np.exp((-(z/sheight)**2) / 2)
    return val


if disk_type == 1:
    sigma = [sigma1(r_list[i]) for i in range(len(r_list))]
if disk_type == 2:
    sigma = [sigma2(r_list[i]) for i in range(len(r_list))]

sc_height = [scale_height(r_list[i]) for i in range(len(r_list))]

ar = np.zeros((len(r_list), len(z_list)))
for i in range(len(r_list)):
    for j in range(len(z_list)):
        ar[i][j] = density(sigma[i],sc_height[i],z_list[j])

ar = np.transpose(ar)
colormax = np.max(ar)
colormin = colormax/1e5
mpl.rcParams['font.size'] = 20
norm = colors.LogNorm()
plt.pcolor(r_list,z_list, ar, vmin = colormin, vmax = colormax, norm = norm, cmap = 'jet')
plt.colorbar()
if ymin is not None and ymax is not None:
    plt.ylim((ymin,ymax))
contours = np.arange(int(np.log10(colormin)),int(np.log10(colormax))+1)
contours = np.power(float(10), contours)
fmt = '%.1e'
cs = plt.contour(r_list,z_list, ar, contours, colors = 'black')
cs.clabel(contours, inline = 1, fmt = fmt, colors = 'black', manual = True)
plt.title('H2 density (cm-3)')
plt.xlabel('R (au)')
plt.ylabel('z (au)')
plt.savefig('h2density.png', bbox_inches = 'tight')
plt.close()

plt.plot(r_list, sigma)
plt.title('Sigma (g.cm-2)')
plt.xlabel('R (au)')
plt.xscale('log')
plt.yscale('log')
plt.savefig('sigma.png', bbox_inches = 'tight')
plt.close()

plt.plot(r_list, sc_height)
plt.title('Scale height (au)')
plt.xlabel('R (au)')
plt.savefig('scale_height.png', bbox_inches = 'tight')
plt.close()

