#!/user/homedir/demarsd/.nix-profile/bin/python

"""

################################
##### This is pl_graphs.py #####
################################

This routine will plot multiple graphs :
    - abundance.fits
    - Temperature.fits
    - CN_density.fits (CN number density)
    - gas_density_true.fits (H2 number density)

-> Note that for H2 number density, it may vary vary a lot from the inner part to the outer part of the disk. Since the maximum value on the colormap is chosen as the maximum value in the fits file, and the minimum color is colormax/1e5, the color range for this graph is likely to be way too high. If this is case, you may re-draw this particular graph using the pl_fits method by setting proper colormin and colormax parameters.

##################################

"""


import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('-dir', '--directory', help = 'name of the model directory', type = str, required = True)
parser.add_argument('-d', '--default', help = 'default parameters file', type = str, required = True)
parser.add_argument('-xmin', help = 'x minimum value on the graph; if you set xmin, you must also set xmax', type =float)
parser.add_argument('-xmax', help = 'x maximum value on the graph; if you set xmax, you must also set xmin', type =float)
parser.add_argument('-ymin', help = 'y minimum value on the graph; if you set ymin, you must also set ymax', type =float)
parser.add_argument('-ymax', help = 'y maximum value on the graph; if you set ymax, you must also set ymin', type =float)
parser.add_argument('-g', '--graph_labels', help = 'manual placement of labels on graph contours', type = bool)
parser.add_argument('-x', '--xscale', help = 'either linear or log', type = str)
parser.add_argument('-y', '--yscale', help = 'either linear, log or flaring. flaring will draw y = z/R', type = str)
parser.add_argument('-c', '--colormap', help = 'name of the colormap', type = str)

args = parser.parse_args()


directory = args.directory
default = args.default
xmin = args.xmin
xmax = args.xmax
ymin = args.ymin
ymax = args.ymax
cmap = args.colormap
labels = args.graph_labels
xscale = args.xscale
yscale = args.yscale


from routines import pl_fits,split_pathnm


default = split_pathnm(default)[2]

names = ['abundance.fits',
          'Temperature.fits',
          'CN_density.fits',
          'gas_density_true.fits']
savenames = ['abundance.png',
             'Temperature.png',
             'CN_density.png',
             'gas_density_true.png']
titles = ['Molecular abundance over the disk',
          'Temperature over the disk (K)',
          'CN density over the disk (cm-3)',
          'Gas density over the disk (cm-3)']
newnames = []
for i in range(len(names)):
    newnames.append(directory  + names[i])
    savenames[i] = directory  + savenames[i]

for i in range(len(names)):
    if names[i] == 'Temperature.fits':
        pl_fits(filename = newnames[i], default_filename = default, savename = savenames[i], grid_file = directory +'grid.fits', colormap = cmap, xscale = xscale, yscale = yscale, xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax, draw_contours = labels, manual_labels = labels, title = titles[i], colormin = 0, colormax = 100)
    else:
        pl_fits(filename = newnames[i], default_filename = default, savename = savenames[i], grid_file = directory +'grid.fits', colormap = cmap, xscale = xscale, yscale = yscale, xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax, draw_contours = labels, manual_labels = labels, title = titles[i])





















