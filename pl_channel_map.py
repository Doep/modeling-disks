#!/user/homedir/demarsd/.nix-profile/bin/python

"""

#####################################
##### This is pl_channel_map.py #####
#####################################

This routine will make a channel map from the given cube
Note that if the cube has a lot of pixels, the generation of the channel map might take some time

##################################

"""


import argparse

parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('-f', '--filename', help = 'fits file from which to make channel map', type = str, required = True)
parser.add_argument('-d', '--default', help = 'default parameters file', type = str)
parser.add_argument('-s', '--savename', nargs ='?', default = 'channelmap.png', help = 'savename for the graph', type = str)
parser.add_argument('-vmin', '--vmin', help = 'minimum speed on the channel map', type = float)
parser.add_argument('-vmax', '--vmax', help = 'maximum speed on the channel map', type = float)
parser.add_argument('-i', '--interv',  help = 'speed interval in the channel map', type = float)
parser.add_argument('-cmin', '--colormin', help = 'minimum value on the channel map color bar', type = float)
parser.add_argument('-cmax', '--colormax', help = 'maximum value on the channel map color bar', type = float)
parser.add_argument('-c', '--colormap',  help = 'name of the colormap', type = str)
parser.add_argument('-l','--linear_scale',  help = 'forces a linear scale on the colormap', action = 'store_true')
parser.add_argument('-t','--textcolor',  help = 'color of the text', type = str)
parser.add_argument('-xmin', help = 'x minimum value on the graph; if you set xmin, you must also set xmax', type =float)
parser.add_argument('-xmax', help = 'x maximum value on the graph; if you set xmax, you must also set xmin', type =float)
parser.add_argument('-ymin', help = 'y minimum value on the graph; if you set ymin, you must also set ymax', type =float)
parser.add_argument('-ymax', help = 'y maximum value on the graph; if you set ymax, you must also set ymin', type =float)
parser.add_argument('-v', '--v_offset', help = 'speed offset on the channel map', type = float)
parser.add_argument('-font', help = 'font size', type = float)

args = parser.parse_args()


filename = args.filename
default = args.default
savename = args.savename
cmin = args.colormin
cmax = args.colormax
interv = args.interv
vmin = args.vmin
vmax = args.vmax
cmap = args.colormap
textcolor = args.textcolor
xmin = args.xmin
xmax = args.xmax
ymin = args.ymin
ymax = args.ymax
v_offset = args.v_offset
font = args.font
force_linear_scale = args.linear_scale


from routines import channel_map,split_pathnm
import os
import numpy as np

if default is not None:
    default = split_pathnm(default)[2]
splited_file = split_pathnm(filename)
file_dir = splited_file[0]

savename = file_dir + savename

channel_map(filename = filename, default_filename = default, force_linear_scale = force_linear_scale, savename = savename, vmin = vmin, vmax = vmax, interv = interv, colormin = cmin, colormax = cmax, colormap = cmap, textcolor = textcolor, xmin = xmin, xmax = xmax, ymin = ymin, ymax = ymax, v_offset = v_offset, font = font)
