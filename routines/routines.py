
import numpy as np
from astropy.io import fits
import os
import subprocess
import shutil
import matplotlib.pyplot as plt
import matplotlib.colors as colors
import matplotlib as mpl
from scipy.interpolate import interp1d
from scipy.ndimage import rotate
from scipy.ndimage import gaussian_filter
from matplotlib.ticker import MultipleLocator
import time


cazzoletti_directory = '/user/homedir/demarsd/Documents/stage/modeles/cazzoletti_extended_datas/'
sls_file = '/user/homedir/demarsd/Documents/lut/sls.lut'

uma_si = 1.6605e-27     # uma value in Kg
uma_cgs = 1.6605e-24    # uma value in g
au_si = 1.496e11        # au value in m
au_cgs = 1.496e13       # au value in cm
M_sun_si = 1.9884e30    # M_sun in Kg
M_sun_cgs = 1.9884e33   # M_sun in g
c_si = 299792458        # c in m/s



#_________________________________________________________________________________
#_________________________________________________________________________________
# Reads default parameters from parameters file
def read_default_parameters(filename = 'default.para'):
    """
    ------------------------------------------------------
    ########################################################
    === Reads parameters from the given filename ===
    ########################################################
    ### Input
        -filename : name of the file from which to load parameters
    ### Output
        - dictionnary containing parameters names as keyword, and their associated value. Values can be integers, floats, strings or booleans.
    ------------------------------------------------------
    """
    default_parameters = {}
    with open(filename) as pf:
        line = pf.readline()
        while line:
            value_list = []
            i = 0
            while line[i] == ' ':
                i+=1
            if line[i] == '#' or line[i] == '\n':
                line = pf.readline()
                continue
            j = i
            while line[j] != '#' and line[j] != '\n':
                j += 1
            line_no_comment = line[i:j]
            split_line = line_no_comment.split('=')
            variable_name = (split_line[0]).split()[0]
            values = (split_line[1]).split()
            for val in values:
                try:
                    val_int = int(val)
                    value_list.append(val_int)
                except:
                    try:
                        val_float = float(val)
                        value_list.append(val_float)
                    except:
                        if val == 'True':
                            value_list.append(True)
                        elif val == 'False':
                            value_list.append(False)
                        elif val == 'None':
                            value_list.append(None)
                        else:
                            value_list.append(val)
            if len(value_list) == 1:
                value_list = value_list[0]
            default_parameters[variable_name] = value_list
            line = pf.readline()
    return default_parameters

#_________________________________________________________________________________
#_________________________________________________________________________________
# Loads default parameters from parameters file
def load_default_parameters(loc,
                            use_defaults = True,
                            filename = 'default.para'):
    """
    ------------------------------------------------------
    ########################################################
    === Reads parameters from the given filename and returns only the parameters where loc[para] is None ===
    ########################################################
    ### Input
        - loc : parameters to give default value to. Their value will given the one from 'filename' if and only if their value is None
        - filename : name of the file from which to load parameters
        - use_defaults : True/False, do you wish to read default parameters. If False, will return loc as it was given
    ### Output
        - dictionnary containing parameters names as keyword, and their associated value. Values can be integers, floats, strings or booleans.
    ------------------------------------------------------
    """
    local_parameters = loc.copy()
    if use_defaults == True:
        defaults = read_default_parameters(filename = filename)
        for para in local_parameters:
            if para in defaults:
                if local_parameters[para] is None:
                    local_parameters[para] = defaults[para]
    return local_parameters


#_________________________________________________________________________________
#_________________________________________________________________________________
# Make MCFOST parameters file
def make_mcfost_para_file(filename = 'default.para',
                          savename = 'mcfost_parameters.para'):
    """
    ------------------------------------------------------
    ########################################################
    === Builds MCFOST parameters file from the template given in 'filename' by the parameter 'template' ===
    ########################################################
    ### Input
        - filename : filename from which to read parameters for the MCFOST file, as well as the template directory
        - savename : save name for the MCFOST parameters file
    ### Output
        - Text file, MCFOST parameters file. Note the file will be created in the same directory as 'filename'
    ------------------------------------------------------
    """
    savename = split_pathnm(filename)[0] + savename
    para = read_default_parameters(filename = filename)
    template = para['template']
    tempfile = open(template,'r')
    mcfostfile = open(savename, 'w')
    for template_line in tempfile:
        line = template_line
        i = 0
        while line[i] == ' ':
            i+=1
        if line[i] == '#' or line[i] == '\n':
            mcfostfile.write(line)
            continue
        j = i
        while line[j] != '#' and line[j] != '\n':
            j += 1
        line_no_comment = line[i:j]
        line = line.replace('#',' ')
        line_no_comment = line_no_comment.split()
        for name in line_no_comment:
            if name in para:
                if type(para[name]) == list:
                    valstr = ''
                    for val in para[name]:
                        valstr = valstr + str(val) + ' '
                else:
                    valstr = str(para[name])
                line = line.replace(name, valstr, 1)
        mcfostfile.write(line)
    tempfile.close()
    mcfostfile.close()
    


#_________________________________________________________________________________
#_________________________________________________________________________________
# Open a fits file, and try to open the corresponding .gz
def open_fits(filename,
              *args,
              **kwargs):
    """
    ------------------------------------------------------
    ########################################################
    === Alias for 'fits.open()' function from astropy.io, will try to open the given fits file, then the correspondng .gz one if it does not exist ===
    ########################################################
    ### Input
        - filename : fits file name to open
        - *args, **kwargs : check astropy.io.fits.open() documentation
    ### Output
        - fits.open(filename)
    ------------------------------------------------------
    """
    try:
        opened = fits.open(filename, *args, **kwargs)
    except:
        opened = fits.open(filename + '.gz', *args, **kwargs)
    return opened



#_________________________________________________________________________________
#_________________________________________________________________________________
# Turns a fits file into a list of its sub arrays
def fits_to_array(filename):
    """
    ------------------------------------------------------
    ########################################################
    === Converts a fits file into an array containing each subarray data ===
    ########################################################
    ### Input
        - filename : fits file name to convert
    ### Output
        - List of numpy arrays
    ### Notes
    Each data should be accessed with fits_to_array(filename)[i], with i being the index number in the list
    Note that this arrays are accessed with array[z_index][r_index] if they are 2D arrays, else the order of the dimensions go in the "reversed" order
    ------------------------------------------------------
    """
    file = open_fits(filename)
    array = []
    for i in range (len(file)):
        array.append(file[i].data)
    file.close()
    return array

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Basically useless
# Lines are counted as starting at 0, won't produce a file if a line's data has more than 2 axis
def fits_to_txtfile(filename):
    array = fits_to_array(filename)
    with open_fits(filename) as file:
        for i in range(len(array)):
            if file[i].header['NAXIS'] <=2:
                np.savetxt(os.path.splitext(filename)[0] + '_data_line_{_of_{}.txt'.format(i,len(array)-1), array[i], delimiter=',')

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING This function is obsolete, use grid_parameters instead and check r[i],z[i] values instead
# Gives parameters of the cell for a given (line, n_r, n_z, n_az) cell. Output : (radius, height)
# WARNING : (line, n_r, n_z, n_az) all start at 0 : 1st line is line=0
def param_cell(line, n_r, n_z, n_az, array):
    r = array[line][0][n_az][n_z][n_r]
    z = array[line][1][n_az][n_z][n_r]
    return [r,z]


#_________________________________________________________________________________
#_________________________________________________________________________________
# Reads grid parameters
# Returns 2D arrays of R and z parameters
def grid_parameters(filename):
    """
    ------------------------------------------------------
    ########################################################
    === Reads 'grid.fits' file and returns the r and z value in each cell ===
    ########################################################
    ### Input
        - filename : grid.fits filename
    ### Output
        - [r,z] : r being a 2D numpy array of the r values in each cell, same for z
    ### Notes
    r and z here are already transposed to be accessed with r[r_index][z_index]
    ------------------------------------------------------
    """
    grid_array = fits_to_array(filename)
    r = np.transpose(grid_array[0][0][0])
    z = np.transpose(grid_array[0][1][0])
    return [r,z]


#_________________________________________________________________________________
#_________________________________________________________________________________
# Turns an array into a fits file. For convenience, use a numpy array type.
def array_to_fits(name,
                  array,
                  subs = None):
    """
    ------------------------------------------------------
    ########################################################
    === Converts an array into a fits file ===
    ########################################################
    ### Input
        - name : name of the resulting fits file (type = str)
        - array : main array that will be the primary HDU of the fits file
        - subs : list of other arrays to be added to the fits file as secondary HDUs
    ### Output
        - fits file
    ### Notes
    ------------------------------------------------------
    """
    hdu = fits.PrimaryHDU(array)
    listhdu = [hdu]
    if subs != None:
        for ar in subs:
            listhdu.append(fits.ImageHDU(ar))
    hdul = fits.HDUList(listhdu)
    hdul.writeto(name,overwrite=True)

#_________________________________________________________________________________
#_________________________________________________________________________________
# Replaces commas with dots in given file
def comma_to_dot(file):
    with open(file, 'r') as f:
        filedata = f.read()
    filedata = filedata.replace(',', '.')
    with open(file, 'w') as f:
        f.write(filedata)

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Basically useless
# Turns file into an array, also changes z/R into z
def file_to_array(file):
    array = np.loadtxt(file, delimiter = ";")
    for i in range (len(array)):
        array[i][1] = array[i][0] * array[i][1]
    return array

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING not really useful anymore, this is too specific
# Reads the chemical structure
# Turns z/R into z
# Applies a resizing factor
# Applies a z resizing factor
def read_chem_structure(file,expand_z,resize_factor):
    rchem,zchem = np.loadtxt(file, delimiter = ";", unpack=True, usecols=[0,1])
    resize = resize_factor
    for i in range(len(rchem)):
        zchem[i] = zchem[i] * rchem[i] * expand_z * resize
        rchem[i] = rchem[i] * resize
    return [rchem,zchem]

#_________________________________________________________________________________
#_________________________________________________________________________________
# Copy header from a fits file to another 
def copy_header(src, 
                dst, 
                ignore_blank = False, 
                ignore_zero_scaling = True):
    """
    ------------------------------------------------------
    ########################################################
    === Copies all headers from a fits file to another ===
    ########################################################
    ### Input
        - src : (str) fits file to copy headers from
        - dst : (str) fits file to copy headers to
        - ignore_blank : (bool) ignore or not BLANK cards in the headers
        - ignore_zero_scaling : (bool) ignore BSCALE/BZERO cards in the headers
    ### Output
        - dst fits file with same headers as src file
    ### Notes
    ------------------------------------------------------
    """
    src_nm = open_fits(src)
    dst_nm = open_fits(dst, 'update')
    hdr_src = src_nm[0].header
    hdr_dst = dst_nm[0].header
    for i in hdr_src:
        try:
            if str(i) == 'BLANK' and ignore_blank == True:
                print('ignoring BLANK card')
            elif (str(i) == 'BSCALE' or str(i) == 'BZERO') and ignore_zero_scaling == True:
                print('ignoring BSCALE / BZERO card')
            elif str(i) == 'DATAMIN':
                hdr_dst[str(i)] = np.min(fits_to_array(dst)[0])
                hdr_dst.comments[str(i)] = hdr_src.comments[str(i)]
            elif str(i) == 'DATAMAX':
                hdr_dst[str(i)] = np.max(fits_to_array(dst)[0])
                hdr_dst.comments[str(i)] = hdr_src.comments[str(i)]
            else:
                hdr_dst[str(i)] = hdr_src[str(i)]
                hdr_dst.comments[str(i)] = hdr_src.comments[str(i)]
        except:
            print('header or comment line : ' + i + ' could not be copied')
    src_nm.close()
    dst_nm.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# Rotates a cube, angle is defined positive in the trigonometric way
def rotate_cube(filename,
                angle):
    """
    ------------------------------------------------------
    ########################################################
    === Rotates a cube by provided angle using the scipy.ndimage.rotate method ===
    ########################################################
    ### Input
        - filename : (str) cube name to rotate
        - angle : (float) angle by which to rotate the cube, defined positive in the trigonometric way
    ### Output
        - fits file named : 'filename_rotated.fits', rotated by given angle
    ### Notes
    ------------------------------------------------------
    """
    angle = - angle
    ar = fits_to_array(filename)[0]
    rot = rotate(ar, angle = angle, axes = (1,2), reshape = False)
    savename = os.path.splitext(filename)[0] + '_rotated.fits'
    array_to_fits(savename, rot)
    copy_header(filename, savename, ignore_zero_scaling = True)

#_________________________________________________________________________________
#_________________________________________________________________________________
# Plots the sign of the difference between two fits files
def fits_diff(filename,
              savename = 'diff.fits'):
    """
    ------------------------------------------------------
    ########################################################
    === Makes a copy of the given cube, containing 0 if value was 0, -1 if value was negative, +1 if value was positive ===
    ########################################################
    ### Input
        - filename : (str) name of the cube to check the diff values
        - savename : (str) save name for the diff cube
    ### Output
        - cube of the same shape as 'filename', containing the sign of the values
    ### Notes
    Useful to check wether there is or is not self-absorption in the model : negative values imply self-absorption
    ------------------------------------------------------
    """
    array = fits_to_array(filename)[0]
    array[array > 0] = 1
    array[array < 0] = -1
    array_to_fits(savename, array)
    subprocess.call('gzip ' + savename, shell = True)




# Make a list of coordinates (in arcsec) from a given cube
# Also makes a cube containing said coordinates if make_cube is set to True
def coordinates(filename, 
                make_cube = False, 
                savename = 'coordinates.fits'):
    """
    ------------------------------------------------------
    ########################################################
    === Reads cube headers and returns a list of physical coordinates of x and y axis in arcsec ===
    ########################################################
    ### Input
        - filename : (str) file name to read
        - make_cube : (bool) set to True if you wish to make  a cube containing the physical coordinates in arcsec
        - savename : (str) save name for the cube (if make_cube is set to True)
    ### Output
        - [xlist, ylist] : (list of 1D lists) contains x and y coordinates in the cube
    ### Notes
    ------------------------------------------------------
    """
    xlist = make_coord_list(filename, axis = 1, no_crval = True)#, abs_cdelt = True)
    ylist = make_coord_list(filename, axis = 2, no_crval = True)#, abs_cdelt = True)
    # ARCSEC coordinates array. pos[0] = x, pos[1] = y
    if make_cube == True:
        ar = fits_to_array(filename)[0]
        pos = np.zeros(np.shape(ar[0]))
        pos = [pos,pos]
        for i in range(xsize):
            for j in range(ysize):
                pos[0][j][i] = xlist[i]
                pos[1][j][i] = ylist[j]
        array_to_fits(savename, pos)
    xlist = np.dot(xlist, 3600)
    ylist = np.dot(ylist, 3600)
    return [xlist,ylist]
    
    

#_________________________________________________________________________________
#_________________________________________________________________________________
# Add an header to a fits file
def add_header(filename,
               hdr_name,
               hdr_content,
               comment = None):
    """
    ------------------------------------------------------
    ########################################################
    === Adds a header to asked filename, with provided value and comment ===
    ########################################################
    ### Input
        - filename : (str) name of the file to add a header to
        - hdr_name : (str) name of the header
        - hdr_content : (str, int, float, ...) value to give to the header
    ### Output
        - fits file with added header
    ### Notes
    ------------------------------------------------------
    """
    nm = open_fits(filename, 'update')
    hdr = nm[0].header
    if comment is None:
        hdr[hdr_name] = hdr_content
    else:
        hdr[hdr_name] = (hdr_content, comment)
    nm.close()



#_________________________________________________________________________________
#_________________________________________________________________________________
# Make the list of value along the given axis
def make_coord_list(filename,
                    axis,
                    no_crval = False,
                    abs_cdelt = False):
    """
    ------------------------------------------------------
    ########################################################
    === Makes a list of physical coordinates from the 'filename', along asked 'axis' ===
    ########################################################
    ### Input
        - filename : (str) name of the file to read coordinates from
        - axis : (int) axis number
        - no_crval : (bool) ignore CRVAL parameter
        - abs_cdelt : (bool) returns the abs(CDELT) card instead of its actual value
    ### Output
        - list of coordinates
    ### Notes
    CHeck the order of the axis before use, if you are looking for the y axis in x,y,z,az, axis number might be 2 as the order might be reversed in the fits file
    ------------------------------------------------------
    """
    lines = open_fits(filename)
    hdr = lines[0].header
    axlength = hdr['NAXIS' + str(axis)]
    if no_crval == False:
        crval = hdr['CRVAL' + str(axis)]
    else:
        crval = 0
    cdelt = hdr['CDELT' + str(axis)]
    if abs_cdelt == True:
        cdelt = abs(cdelt)
    crpix = hdr['CRPIX' + str(axis)]
    coord_list = []
    for i in range(axlength):
        pix_val = crval + (i-(crpix-1))*cdelt
        coord_list.append(pix_val)
    return coord_list


#_________________________________________________________________________________
#_________________________________________________________________________________
# Sum all three rays
def sum_rays(savename = 'sum_hyperfine.fits',
             nu_list = None,
             prefix = 'ray'):
    """
    ------------------------------------------------------
    ########################################################
    === Sums rays with reference frequency being the first entry in nu_list ===
    ########################################################
    ### Input
        - savename : (str) save name for the resulting summed channel map
        - nu_list : (None or list of floats) if None, will read frequencies from 'NU_REF' card in each fits file. Chooses nu_list[0] as reference frequency
        - prefix : (str) prefix for rays. default is 'ray' and will look at all file of the form 'ray#.fits', # being an integer starting at 0 and increasing by 1 on each iteration. Stops when it does not find a file anymore : therefore there shouldn't b 'ray0.fits', 'ray1.fits', 'ray3.fits' as it won't read the latter
    ### Output
        - fits file : channel map with each ray summed with the frequency/speed shift given by first order linearization of the doppler effect.
    ### Notes
    ------------------------------------------------------
    """
    if nu_list is None:
        i = 0
        nu_list = []
        while os.path.isfile(prefix + str(i) + '.fits') == True:
            ray_name = prefix + str(i) + '.fits'
            ray_file = open_fits(ray_name)
            nu_ref = ray_file[0].header['NU_REF']
            nu_list.append(nu_ref)
            ray_file.close()
            i = i + 1
    
    # Removing Nones and NaNs
    for k in range(len(nu_list)):
        if nu_list[k] is None:
            nu_list[k] = np.nan
    test = np.isnan(nu_list)
    ind = []
    for k in range(len(test)):
        if test[k] == True:
            ind.append(k)
    nu_list = np.delete(nu_list, ind)
    
    # Taking nu0 and removing it
    nu0 = nu_list[0]
    nu_list = np.delete(nu_list,0)
    ray_n = len(nu_list)
    
    #Fits to arrays
    rays = []
    for k in range(ray_n +1):
        rays.append(fits_to_array(prefix + str(k) + '.fits')[0])
    
    
    c = c_si /1000  # c in km/s
    
    file_ray0 = prefix + '0.fits'
    fits_ray0 = open_fits(file_ray0)
    slices = fits_ray0[0].header['NAXIS3']       # Number of v slices
    v_interv = fits_ray0[0].header['CDELT3']     # Speed interval between slices
    
    # Entire list of velocities
    #slice_min = -int(slices/2)
    #slice_max = int(slices/2)
    v = make_coord_list(file_ray0, axis = 3)
    
    # Applying Doppler
    v_list = []
    for k in range(ray_n):
        delta_v = -c * (nu_list[k] - nu0)/nu0
        v_list.append(np.subtract(v, delta_v))
    
    v_list = np.array(v_list)
    
    v_min = np.min(v)
    v_max = np.max(v)
    print('vmin,vmax', v_min,v_max)
    # Remove slices out of range
    for k in range(ray_n):
        for i in range(slices):
            if v_list[k][i]<v_min or v_list[k][i]>v_max:
                v_list[k][i] = np.nan


    
    # Get the slices indices
    isnan = []
    indices = []
    for k in range(ray_n):
        indi_k = []
        isnan.append(np.isnan(v_list[k]))
        for i in range(slices):
            if isnan[k][i] == False:
                indi_k.append(find_nearest(v,v_list[k][i]))
            else:
                indi_k.append(np.nan)
        indices.append(indi_k)
    
    # Print corresponding indexes
    for k in range(ray_n):
        print('list v' +str(k +1) + ' :')
        print(indices[k])
    
    # Checks that there are no duplicates slices
    print('_______________________________________________________')
    print('_______________________________________________________')
    list_indices = []
    unique = []
    for k in range(ray_n):
        list_indices.append([indices[k][i] for i in range(slices) if isnan[k][i] == False])
        list_unique, occurences = np.unique(list_indices[k], return_counts = True)
        a = [list_unique, occurences]
        unique.append(a)
    
    for k in range(ray_n):
        print('Checking list v' + str(k+1) + ' for duplicates')
        check = False
        for i in range(len(unique[k][1])):
            if unique[k][1][i] != 1:
                print('index ' + str(unique[k][0][i]) + ' appears ' + str(unique[k][1][i]) + ' times')
                check = True
        if check == False:
            print('No duplicates')
    
    print('_______________________________________________________')
    print('_______________________________________________________')
    
    # Summing rays
    array = np.zeros(np.shape(rays[0]))
    for i in range(slices):
        val = np.zeros(np.shape(rays[0][0]))
        for k in range(ray_n +1):
            if k ==0:
                val = np.add(val,rays[k][i])
            else:
                c = k-1
                if isnan[c][i] == False:
                    val = np.add(val, rays[k][indices[c][i]])
        array[i] = val

    
    array_to_fits(savename, array)
    copy_header(file_ray0,savename)
    file_final = open_fits(savename, 'update')
    hdr = file_final[0].header
    hdr['NU_REF'] = nu0
    file_final.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# Removes continuum from the lines.fits file
def continuum_removal(filename = 'lines.fits', 
                      output_prefix = 'ray'):
    """
    ------------------------------------------------------
    ########################################################
    === Removes the continuum from 'lines.fits' and separates each ray into output_prefix#.fits ===
    ########################################################
    ### Input
        - filename : (str) name of the fits file containing all rays, continuum, ect... should not be anything else than 'lines.fits' unless you know what you are doing
        - output_prefix : (str) output prefix to separate rays
    ### Output
        - diff.fits files for each ray wth continuum subtracted
        - a fits file for each ray with continuum subtracted
    ### Notes
    ------------------------------------------------------
    """
    # Extracting arrays
    lines = open_fits(filename)
    line = fits_to_array(filename)
    rays = line[0]
    continuum = line[1]
    
    # Removing length-1 axis'
    rays = rays[0][0]
    continuum = continuum[0][0]
    
    # Axis parameters
    NAXIS3 = lines[0].header['NAXIS3']
    NAXIS4 = lines[0].header['NAXIS4']
    # Removing continuum
    for i in range(NAXIS4):
        print('step : ' + str(i) + '/'+str(NAXIS4 -1))
        channel_ray       = rays[i]
        channel_continuum = continuum[i]
        temp_ray = []
        for k in range(NAXIS3):
            #print('step : ' + str(i) + '/'+str(NAXIS4 -1) + ', ' +str(k) + '/' +str(NAXIS3 -1))
            val = channel_ray[k] - channel_continuum
            temp_ray.append(val)
        savename = output_prefix + str(i) + '.fits'
        savename_diff = 'diff_' + savename
        array_to_fits(savename, temp_ray)
        fits_diff(savename, savename = savename_diff)
        # Adding headers
        copy_header(filename, savename)
        copy_header(savename, savename_diff)
        # Adding reference frequency header
        nu_ref = line[3][i]
        ray_file = open_fits(savename, 'update')
        hdr = ray_file[0].header
        hdr['NU_REF'] = nu_ref
        ray_diff_file = open_fits(savename_diff, 'update')
        hdr_diff = ray_diff_file[0].header
        hdr_diff['NU_REF'] = nu_ref
        ray_file.close()
        ray_diff_file.close()
        

#_________________________________________________________________________________
#_________________________________________________________________________________
# Reads a fits file and plot it
def pl_fits(filename,
            savename,
            draw_other_contours = False,
            other_contours = [10,20,30],
            other_filename = 'Temperature.fits',
            use_defaults = True,
            default_filename = 'default.para',
            colormin = None,
            colormax = None,
            colormap = None,
            xscale = None,
            yscale = None,
            xmin = None,
            xmax = None,
            ymin = None,
            ymax = None,
            title = None,
            draw_contours = None,
            c_interv = None,
            manual_labels = None,
            map_scale = None,
            grid_file = 'grid.fits',
            font = None,
            savefig = True,
            show = False,
            close = True):
    """
    ------------------------------------------------------
    ########################################################
    === Reads a 2D image fits file and plot it ===
    ########################################################
    README : most parameters can be given None value. in this case, parameters will be taken from 'default_filename' if 'use_default' is True, else python will handle the graph automatically. Note
    ########################################################
    ### Input
        - filename : (str) file name to draw
        - savename : (str) save name for the figure
        - draw_other_contours : (bool) if True, will 'other_contours' from 'other_filename' on top of the figure
        - other_contours : (list) other contours to draw
        - other_filename : (str) other filename from which to draw 'other_contours'
        - use_defaults : (bool) use or not default parameters from 'default_filename'
        - default_filename : (str) default filename to read default parameters from
        - colormin : (float) minimum value on the colormap (if None, will be colormax/1e5)
        - colormax : (float) maximum value on the colormap (if None, will be maximum value in the graph)
        - xscale : 'log' or 'linear', scaling of the xaxis
        - yscale : 'linear' or 'flaring' or 'log', scaling of the yaxis. 'flaring' will draw z/R
        - xmin : (float) minimum value on the x axis. WARNING should not be 0 if xscale == 'log'
        - xmax : (float) maximum value on the x axis
        - ymin : (float) minimum value on the y axis. WARNING should not be 0 if yscale == 'log'
        - ymax : (float) maximum value on the y axis
        - title : (str) title to give to the graph
        - draw_contours : (bool) draw or not contours on the graph
        - c_interv : (float) contours interval if the colorbar is on a linear scale
        - manual_labels : (bool) manual placement of labels on contours HIGHLY ADVISED TO SET TO TRUE
        - map_scale : 'log' or 'linear', scale of the colormap
        - grid_file : (str) name of the 'grid.fits' file
        - font : (int) font size
        - savefig : (bool) save figure at the end of the execution
        - show : (bool) show the figure at the end of the execution (WARNING setting this to True will show the figure but kill it)
        - close : (bool) closes the figure at the end of the execution (set this to False if you wish to draw other things on top of the graph)
    ### Output
        - savename(.png) image file
    ### Notes
    Setting colormin to 0 will force the colormap scale to a linear one
    Placing labels manually : place with left click, finish with scroll-wheel click
    ------------------------------------------------------
    """
    font_fit = font
    colormap_fit = colormap
    new_para = load_default_parameters(locals(), use_defaults = use_defaults, filename = default_filename)
    colormap = new_para['colormap_fit']
    xscale = new_para['xscale']
    yscale = new_para['yscale']
    xmin = new_para['xmin']
    xmax = new_para['xmax']
    ymin = new_para['ymin']
    ymax = new_para['ymax']
    draw_contours = new_para['draw_contours']
    manual_labels = new_para['manual_labels']
    font = new_para['font_fit']
    c_interv = new_para['c_interv']
    
    if draw_contours is None:
        draw_contours = False
    if manual_labels is None:
        manual_labels = True
    if c_interv is None:
        c_interv = 10
    if font is None:
        font = 13
    
    
    r,z = grid_parameters(grid_file)
    data = fits_to_array(filename)[0]
    data = np.transpose(data)
    
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    
    if ymin is None:
        ymin = 0
    if ymax is None:
        if yscale == 'flaring':
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    
    if yscale == 'flaring':
        y_z = z/r
    else:
        y_z = z
    
    if colormax is None:
        colormax = np.max(data)
    if colormin is None:
        colormin = colormax / 1e4
    
    mpl.rcParams['font.size'] = font
    if np.sign(colormin) == np.sign(colormax) or map_scale == 'log':
        norm = colors.LogNorm()
        lbl = 1
    if np.sign(colormin) != np.sign(colormax) or map_scale == 'linear':
        norm = colors.Normalize()
        lbl = 2
    
    plt.pcolor(r, y_z, data, vmin = colormin, vmax = colormax, norm = norm, cmap = colormap)
    plt.colorbar()
    
    if title is None:
        title = os.path.split(filename)[1]
    plt.title(title)
    
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if yscale == 'flaring':
        plt.ylabel('z/R')
    else:
        plt.ylabel('z (au)')
        plt.yscale(yscale)
    if draw_contours == True:
        if lbl == 1:
            contours = np.arange(int(np.log10(colormin)),int(np.log10(colormax))+1)
            contours = np.power(float(10), contours)
            fmt = label_formating(contours)
        if lbl == 2:
            contours = np.arange(colormin,colormax, c_interv)
            fmt = label_formating(contours)
        cs = plt.contour(r, y_z, data, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = fmt, colors = 'black', manual = manual_labels)
    if draw_other_contours == True:
        contours = [float(other_contours[i]) for i in range(len(other_contours))]
        fmt = label_formating(contours)
        other_data = np.transpose(fits_to_array(other_filename)[0])
        cs = plt.contour(r, y_z, other_data, contours, colors = 'white')
        cs.clabel(contours, inline = 1, fmt = fmt, colors = 'white', manual = manual_labels)
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if show == True:
        plt.show()
    if close == True:
        plt.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# Returns the formatting for labels depending on their type : int formating, float formating, exp formating
def label_formating(contours):
    """
    ------------------------------------------------------
    ########################################################
    === Automating formating for contours labels ===
    ########################################################
    ### Input
        - contours : (list) list of contours values
    ### Output
        - string corresponding to the formating type : '%.1e', '%d', '%.2f'
    ### Notes

    ------------------------------------------------------
    """
    flag_int = True
    flag_exp = False
    for cont in contours:
        if int(cont) != cont:
            flag = False
        if np.log10(abs(cont)) > 3 or np.log10(abs(cont))<0:
            flag_exp = True
    if flag_exp == True:
        fmt = '%.1e'
    elif flag_int == True:
        fmt = '%d'
    else:
        fmt = '%.2f'
    return fmt

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Obsolete, use pl_fits instead
# Reads abundance fits file and plot it on a graph
def pl_abundance(filename = 'abundance.fits', savename = 'abundance_graph.png', colormin = 1e-13, colormax = 1e-06, colormap = 'gist_rainbow_r', xscale = 'log', xmin = None, xmax = None, ymin = None, ymax = None, draw_contours = False, manual_labels = False, grid_file = 'grid.fits', plot_z = False):
    r,z = grid_parameters(grid_file)
    ab = fits_to_array(filename)[0]
    ab = np.transpose(ab)
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    if ymin is None:
        ymin = 0
    if ymax is None:
        if plot_z == False:
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    if plot_z == False:
        y_z = z/r
    else:
        y_z = z
    plt.pcolor(r, y_z, ab, vmin = colormin, vmax = colormax, norm = colors.LogNorm(), cmap = colormap)
    plt.colorbar()
    plt.title('Molecular abundance over the disk')
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if plot_z == False:
        plt.ylabel('z/R')
    else:
        plt.ylabel('z')
    contours = np.arange(int(np.log10(colormin)),int(np.log10(colormax))+1)
    contours = np.power(float(10), contours)
    if draw_contours == True:
        cs = plt.contour(r, y_z, ab, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = '%.1e', colors = 'black', manual = manual_labels)
    plt.savefig(savename)
    plt.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING WARNING WARNING Dot not use pl_fits to plot the temperature, you should really use this function instead, as the colorbar scale is more specific here
# Reads temperature fits file and plot it on a graph
def pl_temperature(filename = 'temperature.fits', 
                   savename = 'temperature.png', 
                   colormin = 0, 
                   colormax = 200, 
                   colormap = 'gist_rainbow_r', 
                   xscale = 'log', 
                   xmin = None, 
                   xmax = None, 
                   ymin = None, 
                   ymax = None, 
                   draw_contours = False, 
                   manual_labels = False, 
                   grid_file = 'grid.fits', 
                   plot_z = False, 
                   c_interv = 10, 
                   font = 15, 
                   savefig = True, 
                   show = False, 
                   close = True):
    mpl.rcParams['font.size'] = font
    r,z = grid_parameters(grid_file)
    temp = fits_to_array(filename)[0]
    temp = np.transpose(temp)
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    if ymin is None:
        ymin = 0
    if ymax is None:
        if plot_z == False:
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    if plot_z == False:
        y_z = z/r
    else:
        y_z = z
    plt.pcolor(r, y_z, temp, vmin = colormin, vmax = colormax, cmap = colormap)
    plt.colorbar()
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if plot_z == False:
        plt.ylabel('z/R')
    else:
        plt.ylabel('z')
    contours = np.arange(10,colormax+1, c_interv)
    if draw_contours == True:
        cs = plt.contour(r, y_z, temp, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = '%d', colors = 'black', manual = manual_labels)
    plt.title('Temperature over the disk')
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if show == True:
        plt.show()
    if close == True:
        plt.close()

#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING Obsolete, use pl_fits instead
# Reads gas density, and CN abundance, then draws CN density
def pl_CN_density(savename = 'CN_density.png', ab_filename = 'abundance.fits', dens_filename = 'gas_density.fits', grid_filename = 'grid.fits', xmin = None, xmax = None, ymin = None, ymax = None, xscale = 'log', colormap = 'gist_rainbow_r', plot_z = False, draw_contours = False, manual_labels = False, colormin = None, colormax = None):
    r,z = grid_parameters(grid_filename)
    ab = fits_to_array(ab_filename)[0]
    ab = np.transpose(ab)
    dens = fits_to_array(dens_filename)[0]
    dens = np.transpose(dens)
    uma = uma_cgs
    au = au_cgs
    mu = 2.8
    dens = dens/(mu*uma)
    n_CN = np.multiply(ab,dens)
    array_to_fits('CN_density.fits', np.transpose(n_CN))
    if xmin is None:
        xmin = 1
    if xmax is None:
        xmax = np.max(r)
    if ymin is None:
        ymin = 0
    if ymax is None:
        if plot_z == False:
            ymax = np.max(z/r)
        else:
            ymax = np.max(z)
    if plot_z == False:
        y_z = z/r
    else:
        y_z = z
    
    if colormax is None:
        colormax = np.max(n_CN)
    if colormin is None:
        colormin = colormax / (1e9)
    plt.pcolor(r, y_z, n_CN, vmin = colormin, vmax = colormax, norm = colors.LogNorm(), cmap = colormap)
    plt.colorbar()
    if xscale != None:
        plt.xscale(xscale)
    plt.xlim((xmin,xmax))
    plt.ylim((ymin,ymax))
    plt.xlabel('R (au)')
    if plot_z == False:
        plt.ylabel('z/R')
    else:
        plt.ylabel('z')
    contours = np.arange(int(np.log10(colormin)),int(np.log10(colormax))+1)
    contours = np.power(float(10), contours)
    if draw_contours == True:
        cs = plt.contour(r, y_z, n_CN, contours, colors = 'black')
        cs.clabel(contours, inline = 1, fmt = '%.1e', colors = 'black', manual = manual_labels)
    #plt.show()
    plt.title('CN density over the disk')
    plt.savefig(savename)
    plt.close()



#_________________________________________________________________________________
#_________________________________________________________________________________
# Computes the H2 number density, and the CN number density
def compute_density(dens_filename = 'gas_density.fits',
                   ab_filename = 'abundance.fits',
                   mu = 2.8):
    """
    ------------------------------------------------------
    ########################################################
    === Computes the number density of H2 and CN according to density and abundances fits files ===
    ########################################################
    ### Input
        - dens_filename : (str) mass density fits file from MCFOST
        - ab_filename : (str) abundance fits file
        - mu : (float) mu value to compute the number density
    ### Output
        - fits files containing the number density of H2 and CN
    ### Notes

    ------------------------------------------------------
    """
    ab = fits_to_array(ab_filename)[0]
    dens = fits_to_array(dens_filename)[0]
    dens = np.multiply(dens, 1/(mu*uma_cgs))
    n_CN = np.multiply(dens, ab)
    array_to_fits(split_pathnm(dens_filename)[0] + 'gas_density_true.fits', dens)
    array_to_fits(split_pathnm(dens_filename)[0] + 'CN_density.fits', n_CN)




#_________________________________________________________________________________
#_________________________________________________________________________________
# Create the abundance.fits file from Cazzoletti grid
def abundance_cazz(directory = './',
                   savename = 'abundance.fits',
                   use_defaults = True,
                   default_filename = 'default.para',
                   resize = None,
                   zexpand = None,
                   sigma_resize = None,
                   abmax_factor = None,
                   cazz_extend_dir = None,
                   cazz_extend_up = 'cazzoletti_extended_up.txt',
                   cazz_extend_low = 'cazzoletti_extended_low.txt',
                   cazz_extend_abun = 'cazzoletti_extended_abun.txt',
                   dary_mode = False,
                   dary_dir = '/user/homedir/demarsd/Documents/stage/data/dary/CN_scale_height.txt',
                   reduce_at_temp = None,
                   reduce_radius = 100,
                   reduce_at_file = 'Temperature.fits',
                   mcfost_filename = 'J1628_COmodel.para',
                   grid_filename ='data_disk/grid.fits'):
    """
    ------------------------------------------------------
    ########################################################
    === Creates the abundance.fits file from Cazzoletti distribution ===
    ########################################################
    ### Input
        - directory : (str) directory in which to create the fits file
        - savename : (str) save name for the abundance file (you should not change this as 'abundance.fits' is required by MCFOST
        - use_defaults : (bool) use or not default parameters from 'default_filename'
        - default_filename : (str) default filename to read default parameters from
        - resize : (float) r and z resizing parameter for the distribution (typically, 0.5 for J1628)
        - zexpand : (float) z-only resizing parameter for the distribution
        - sigma_resize : (float) applied to the sigma of the distribution (see how the distribution is computed for details)
        - abmax_factor : (float) factor by which to multiply the abundance
        - cazz_extend_dir : (str) directory towards cazzoletti distribution files
        - cazz_extend_up/low/abun : (str) these files represent the upper and lower contours of X = 1e-8, as well as the normalization factor K(r)
        - dary_mode : (bool) the gaussian will be centered in the location in dary scale heights. can be used to re-center the guassian in any position
        - dary_dir : (str) position to recenter the gaussian to. should be two columns -file : r and z
        - reduce_at_temp : (float) temperature et which to reduce the vertical profile
        - reduce_radius : (float) radius at which to reduce the vertical profile
        - reduce_at_file : (str) file to use for the vertical shrinking
        - mcfost_filename : (str) MCFOST parameters file to use for the MCFOST modeling
        - grid_filename : (str) grid.fits directory and file name. should not be changed unless you know what you're doing
    ### Output
        - abundance.fits file, containing the molecular abundance X = n(CN)/n(H2) in each cell
    ### Notes

    ------------------------------------------------------
    """
    new_para = load_default_parameters(locals(), use_defaults = use_defaults, filename = default_filename)
    resize = new_para['resize']
    sigma_resize = new_para['sigma_resize']
    zexpand = new_para['zexpand']
    abmax_factor = new_para['abmax_factor']
    cazz_extend_dir = new_para['cazz_extend_dir']
    cazz_extend_up = new_para['cazz_extend_up']
    cazz_extend_low = new_para['cazz_extend_low']
    cazz_extend_abun = new_para['cazz_extend_abun']
    
    check = read_default_parameters(filename = default_filename)
    if check['disk_type'] == 2:
        abmax_factor = abmax_factor * (check['rc'] / check['r0'])**check['p1']
    
    
    if dary_mode == True and reduce_at_temp != None:
        print('Cannot have dary mode and reduce_at_temp mode at the same time')
        exit(0)
    # Cazzoletti parameters, and converting z/R into z
    r_cazznorm, ab_cazznorm         = np.loadtxt(cazz_extend_dir + cazz_extend_abun, delimiter = ";", unpack=True, usecols=[0,1])
    r_extended_up, z_extended_up    = np.loadtxt(cazz_extend_dir + cazz_extend_up, delimiter = ";", unpack=True, usecols=[0,1])
    r_extended_low, z_extended_low  = np.loadtxt(cazz_extend_dir + cazz_extend_low, delimiter = ";", unpack=True, usecols=[0,1])

    # Cazzoletti peak abundance value and contour value
    cazz_abmax = 1.4e-7
    abcontour = 1e-8
    
    # Reads grid structure
    grid_filename = directory + grid_filename
    [r_grid,z_grid] = grid_parameters(grid_filename)
    r_grid_max = len(r_grid)
    z_grid_max = len(r_grid[0])
    
    # Compute grid limits and inner cell subdivision
    rf = r_grid[r_grid_max -1][0]
    rff = r_grid[r_grid_max -2][0]
    q = rf/rff
    r0 = r_grid[0][0]
    r1 = r0*q
    n_int = 0
    while r_grid[n_int][0] < r1:
        n_int += 1
    print('n_int = ' +str(n_int))
    r_out = int(r0 * q**(r_grid_max - n_int +1))
    print('r_out = ' + str(r_out))
    
    # Compute resizing factor if not provided
    if resize is None:
        resize = r_out / np.max(r_cazznorm)
    
    # Apply resizing parameters
    r_cazznorm      = np.dot(r_cazznorm, resize)
    r_extended_up   = np.dot(r_extended_up, resize)
    r_extended_low  = np.dot(r_extended_low, resize)
    z_extended_up   = np.dot(z_extended_up, resize * zexpand)
    z_extended_low  = np.dot(z_extended_low, resize * zexpand)
    cazz_abmax      = cazz_abmax * abmax_factor
    abcontour       = abcontour * abmax_factor
    
    # Interpolations
    f_cazznorm    = interp1d(r_cazznorm, ab_cazznorm, kind = 'linear')
    f_contour_up  = interp1d(r_extended_up, z_extended_up, kind = 'linear')
    f_contour_low = interp1d(r_extended_low, z_extended_low, kind = 'linear')
    if dary_mode == True:
        r_dary, z_dary = np.loadtxt(dary_dir, delimiter = ";", unpack = True, usecols = [0,1])
        rmin_dary = np.min(r_dary)
        rmax_dary = np.max(r_dary)
        f_dary = interp1d(r_dary, z_dary, kind = 'linear')
    
    # Abundance grid
    abundance = np.zeros((r_grid_max, z_grid_max))
    
    # Interpolations limits
    rmin = [np.min(r_cazznorm), np.min(r_extended_low), np.min(r_extended_up)]
    rmax = [np.max(r_cazznorm), np.max(r_extended_low), np.max(r_extended_up)]
    if dary_mode == True:
        rmin.append(rmin_dary)
        rmax.append(rmax_dary)
    rmin = np.max(rmin)
    rmax = np.min(rmax)
    print('rmin = ' +str(rmin))
    print('rmax = ' +str(rmax))
    
    flag = True
    # resize distribution to match iso-temperature and max abundance at reduce_radius radius
    if reduce_at_temp != None:
        reduce_size = 1001
        z_reduce = np.linspace(0,r_out,reduce_size)
        ab_reduce = np.zeros(reduce_size)
        zu = f_contour_up(reduce_radius)
        zl = f_contour_low(reduce_radius)
        abmax = f_cazznorm(reduce_radius) * cazz_abmax
        dz = zu - zl
        zmax = (zl + zu)/2
        adjust = (1.+0.15/(1.+(reduce_radius/100)**2.2))
        sigma = sigma_resize * adjust * dz/np.sqrt(8*np.log(abmax/abcontour))
        for i in range(reduce_size):
            ab_reduce[i] = abmax*np.exp(-(z_reduce[i]-zmax)**2/2./sigma**2)
        abmax_idx = np.unravel_index(ab_reduce.argmax(), ab_reduce.shape)
        abmax_height = z_reduce[abmax_idx]
        subprocess.call('mcfost ' + mcfost_filename, shell = True)
        subprocess.call('gunzip data_th/' + reduce_at_file, shell = True)
        temp = np.transpose(fits_to_array('data_th/' + reduce_at_file)[0])
        r_list = [r_grid[i][0] for i in range(len(r_grid))]
        r_nearest_idx = find_nearest(r_list, reduce_radius)
        r_nearest_value = r_list[r_nearest_idx]
        # Making Temperature list, and heights list at corresponding radius (reduce_radius)
        if r_nearest_value >= reduce_radius:
            z1 = z_grid[r_nearest_idx-1]
            z2 = z_grid[r_nearest_idx]
            r1 = r_list[r_nearest_idx-1]
            r2 = r_list[r_nearest_idx]
            zmin1_idx = find_smallest_superior(z1, z2[0])
            zmax2_idx = find_highest_inferior(z2, z1[-1])
            grid_factor = (reduce_radius - r1)/(r2 - r1)
            heights_list = []
            temp_list = []
            for i in range(zmin1_idx,zmax2_idx+1):
                height_factor = (z1[i] - z2[i-zmin1_idx])/(z2[i-zmin1_idx +1] - z2[i-zmin1_idx])
                temp1 = temp[r_nearest_idx-1][i]
                temp2 = temp[r_nearest_idx][i-zmin1_idx] + height_factor * (temp[r_nearest_idx][i-zmin1_idx+1] - temp[r_nearest_idx][i-zmin1_idx])
                val = temp1 + grid_factor * (temp2 - temp1)
                heights_list.append(z1[i])
                temp_list.append(val)
        else:
            z1 = z_grid[r_nearest_idx]
            z2 = z_grid[r_nearest_idx+1]
            r1 = r_list[r_nearest_idx]
            r2 = r_list[r_nearest_idx+1]
            zmin1_idx = find_smallest_superior(z1, z2[0])
            zmax2_idx = find_highest_inferior(z2, z1[-1])
            grid_factor = (reduce_radius - r1)/(r2 - r1)
            heights_list = []
            temp_list = []
            for i in range(zmin1_idx,zmax2_idx+1):
                height_factor = (z1[i] - z2[i-zmin1_idx])/(z2[i-zmin1_idx +1] - z2[i-zmin1_idx])
                temp1 = temp[r1][z1[i]]
                temp2 = temp[r_nearest_idx][i-zmin1_idx] + height_factor * (temp[r_nearest_idx][i-zmin1_idx+1] - temp[r_nearest_idx][i-zmin1_idx])
                val = temp1 + grid_factor * (temp2 - temp1)
                heights_list.append(z1[i])
                temp_list.append(val)
        
        if not (np.min(temp_list) < reduce_at_temp and np.max(temp_list) > reduce_at_temp):
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('There is no T = {:.1f}K at radius r = {:.1f}. Distribution will not be scaled accordingly.'.format(reduce_at_temp, reduce_radius))
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
            time.sleep(3.5)
            flag = False
        
        if flag == True:
            for tempmax_idx in range(len(temp_list)):
                if temp_list[tempmax_idx] > reduce_at_temp:
                    break

            temp_factor = (reduce_at_temp - temp_list[tempmax_idx -1])/(temp_list[tempmax_idx]-temp_list[tempmax_idx-1])
            tempmax_height = heights_list[tempmax_idx-1] + temp_factor * (heights_list[tempmax_idx]-heights_list[tempmax_idx-1])
            z_factor = tempmax_height/abmax_height
            print('__________________')
            print(abmax_height)
            print(tempmax_height)
            print(z_factor)
            print('__________________')
            # New resizing z factor
            z_extended_up   = np.dot(z_extended_up, z_factor)
            z_extended_low  = np.dot(z_extended_low, z_factor)
            # New interpolations
            f_contour_up  = interp1d(r_extended_up, z_extended_up, kind = 'linear')
            f_contour_low = interp1d(r_extended_low, z_extended_low, kind = 'linear')
        shutil.rmtree('data_th/',ignore_errors=True)
    
    # Building abundance map
    for i in range(r_grid_max):
        radius = r_grid[i][0]
        #print('cell : ' + str(i) + '/' +str(r_grid_max-1))
        if radius > rmin and radius < rmax:
            zu = f_contour_up(radius)
            zl = f_contour_low(radius)
            abmax = f_cazznorm(radius) * cazz_abmax
            dz = zu - zl
            zmax = (zl + zu)/2
            if dary_mode == True:
                zmax = f_dary(radius)
            adjust = (1.+0.15/(1.+(radius/100)**2.2))
            sigma = sigma_resize * adjust * dz/np.sqrt(8*np.log(abmax/abcontour))
            abundance[i] = abmax*np.exp(-(z_grid[i]-zmax)**2/2./sigma**2)
    
    # Saving abundance map
    abundance = np.transpose(abundance)
    array_to_fits(directory + savename, abundance)
    if flag == False:
        return 1


#_________________________________________________________________________________
#_________________________________________________________________________________
# WARNING old
# Creates the abundance.fits file by placing CN at given abundance if temperature is between given values
def abundance_temp(directory = './',
                   savename = 'abundance.fits',
                   temp_min = 15,
                   temp_max = 25,
                   ab_value = 1e-8):
    temp_filename = directory + 'data_th/Temperature.fits'
    temp = fits_to_array(temp_filename)[0]
    ab = np.zeros(np.shape(temp))
    for i in range(len(temp)):
        for j in range(len(temp[0])):
            t = temp[i][j]
            if t < temp_max and t > temp_min:
                ab[i][j] = ab_value
    array_to_fits(savename, ab)

#_________________________________________________________________________________
#_________________________________________________________________________________
# Creates the abundance.fits file by placing CN at given abundance if temperature is between given values

def abundance_temp_bis(directory = './',
                   savename = 'abundance.fits',
                   temp_min = 17,
                   temp_max = 23,
                   height = 100,
                   ab_value = 1e-8):
    """
    ------------------------------------------------------
    ########################################################
    === Creates the abundance.fits file with constant abundance whenever the temperature is between given values ===
    ########################################################
    ### Input
        - directory : (str) directory in which mcfost runs
        - savename : (str) savename for the abundance fits file, do not change unless you know what you are doing
        - temp_min : (float) minimum temperature
        - temp_max : (float) maximum temperature
        - ab_value : (float) abundance value to set
    ### Output
        - abundance.fits file
    ### Notes

    ------------------------------------------------------
    """
    temp_filename = directory + 'data_th/Temperature.fits'
    grid_filename = directory + 'data_disk/grid.fits'
    temp = fits_to_array(temp_filename)[0]
    temp = np.transpose(temp)
    [r,z] = grid_parameters(grid_filename)
    ab = np.zeros(np.shape(temp))
    
    contours = [(temp_min + temp_max)/2]
    
    cs = plt.contour(r,z,temp,contours)

    data = cs.allsegs
    
    for i in range(len(r)):
        for j in range(len(r[0])):
            t_val = temp[i][j]
            if t_val <= temp_max and t_val >= temp_min and z[i][j] <= height:
                ab[i][j] = ab_value
    
    array_to_fits('abundance.fits',np.transpose(ab))



#_________________________________________________________________________________
#_________________________________________________________________________________
# Makes the channel map
def channel_map(filename = 'lines.fits',
                savename = 'channelmap.png',
                use_defaults = True,
                default_filename = 'default.para',
                vmin = None,
                vmax = None,
                interv = None,
                colormin = None,
                colormax = None,
                colormap = None,
                force_linear_scale = None,
                sls_file = None,
                textcolor = None,
                xmin = None,
                xmax = None,
                ymin = None,
                ymax = None,
                font = None,
                v_offset = 0,
                savefig = True,
                show = False,
                close = True):
    """
    ------------------------------------------------------
    ########################################################
    === Draws a channel map ===
    ########################################################
    ### Input
        - filename : (str) cube from which to draw the channel map
        - savename : (str) save name fo rthe channel map
        - use_defaults : (float) use default parameters
        - default_filename : (str) default parameters file
        - vmin : (float) minimum speed on the channel map
        - vmax : (float ) maximum speed on the channel map
        - interv : (float) speed interval for the channel map
        - colormin : (float) minimum value on the colormap (setting this to zero will force a linear colormap scale)
        - colormax : (foat) maximum value on the colormap
        - colormap : (str) colormap to use, default is 'sls'
        - force_linear_scale : (bool) force linear scale on the colormap
        - sls_file : (str) sls.lut file directory, if you wish to use it
        - textcolor : (str) color to use for the speed text, default is red
        - xmin : (float) minimum value on the x axis
        - xmax : (float) maximum value on the x axis
        - ymin : (float) minimum value on the y axis
        - ymax : (float) maximum value on the y axis
        - font : (int) font size
        - v_offset : (float) if you wish to apply a speed offset to the channel map
        - savefig : (bool) if you wish to save the figure at the end
        - show : (bool) if you wish to show the figure at the end
        - close : (bool) if you wish to close the figure at the end
        
    ### Output
        - channelmap.png : channel map image
    ### Notes
    if you set vmin and vmax t othe same value, it will only draw one slice
    The channel map will always be plotted with as many columns as rows if possilble. Else, there will be one more row than there are columns
    ------------------------------------------------------
    """
    if v_offset is None:
        v_offset = 0
    colormin_ch = colormin
    colormax_ch = colormax
    colormap_ch = colormap
    xmin_ch = xmin
    xmax_ch = xmax
    ymin_ch = ymin
    ymax_ch = ymax
    font_ch = font
    new_para = load_default_parameters(locals(), use_defaults = use_defaults, filename = default_filename)
    vmin = new_para['vmin']
    vmax = new_para['vmax']
    interv = new_para['interv']
    force_linear_scale = new_para['force_linear_scale']
    colormin = new_para['colormin_ch']
    colormax = new_para['colormax_ch']
    colormap = new_para['colormap_ch']
    sls_file = new_para['sls_file']
    textcolor = new_para['textcolor']
    xmin = new_para['xmin_ch']
    xmax = new_para['xmax_ch']
    ymin = new_para['ymin_ch']
    ymax = new_para['ymax_ch']
    font = new_para['font_ch']
    
    
    R,V,B = np.loadtxt(sls_file, delimiter = " ", unpack=True, usecols=[0,1,2])
    a = len(R)
    my_map = []
    for i in range(a):
        my_map.append((R[i],V[i],B[i]))
    cm = colors.LinearSegmentedColormap.from_list('sls',my_map)
    if colormap == 'sls':
        colormap = cm
        
    # Read fits file parameters
    ray = open_fits(filename)
    
    [x,y] = coordinates(filename)
    
    ray = fits_to_array(filename)[0]
    
    # Entire list of velocities
    v_list_tot = make_coord_list(filename = filename, axis = 3)
    v_list_tot = np.add(v_list_tot, v_offset)
    # Adding v_lsr
    #try:
        #v_lsr = open_fits(filename)[0].header['VELO-LSR']
    #except:
        #print('####################')
        #print('VELO-LSR keyword does not exist in this cube, will apply a v = 0 offset instead')
        #print('####################')
        #v_lsr = 0
    v_lsr = read_v_lsr(filename)
    v_list_tot = np.add(v_list_tot, v_lsr)
    
    # Check if entry parameters are correct
    if (vmin not in v_list_tot) or (vmax not in v_list_tot):
        vmin = v_list_tot[find_nearest(v_list_tot, vmin)]
        vmax = v_list_tot[find_nearest(v_list_tot, vmax)]
        print('vmin or vmax not in vlist, vmin = {:.3f} and vmax = {:.3f} will be used'.format(vmin,vmax))
    
    # Plotted list of velocities
    if vmin == vmax:
        v_list = [vmin]
    else:
        v_list = np.round(np.arange(vmin,vmax,interv),3)
    if v_list[-1] != vmax:
        v_list = np.append(v_list,vmax)
    
    slice_list = []

    for i in v_list:
        slice_list.append(find_nearest(v_list_tot,i))
    
    numb_sl = len(slice_list)
    
    print('# Slices list :', slice_list)
    print('# Speed list :', v_list)
    # Dimensions of the channel map
    # Number of rows and columns. Making a square if possible, else, n_rows = n_columns +1
    numb_sl = len(slice_list)
    n_columns = np.sqrt(numb_sl)
    if n_columns == int(n_columns):
        n_columns = int(n_columns)
        n_rows = n_columns
    else:
        n_rows = int(n_columns) +1
        n_columns = int(np.round(n_columns))
    
    # Color limits

    if colormax is None:
        colormax = np.max(ray)

    if colormin is None:
        colormin = colormax / 1e3
    
    # Plotting channelmap
    if np.sign(colormin) == np.sign(colormax):
        norm = colors.LogNorm()
    else:
        norm = colors.Normalize()
    if force_linear_scale == True:
        norm = norm = colors.Normalize()
    
    print('# numb_sl', numb_sl)
    
    print('# ncolumns', n_columns)
    fig_size = 30
    if font == None:
        font = 20
    speed_font = 0.8*int(font * fig_size/20 * 6/n_columns)
    axes_font = 0.8*speed_font
    mpl.rcParams['font.size'] = axes_font
    fig, axes = plt.subplots(n_rows,n_columns, sharex = True, sharey = True, figsize = (fig_size,fig_size))
    for i in range(numb_sl):
        print(i)
        xplot = i//n_columns
        yplot = i%n_columns
        print(xplot, yplot)
        if n_rows == 1 and n_columns ==1:
            ax = axes
        elif n_rows == 1 or n_columns == 1:
            ax = axes[yplot]
        else:
            ax = axes[xplot, yplot]
        ar = ray[slice_list[i]]
        im = ax.pcolor(x,y,ar, norm =  norm, vmin = colormin, vmax = colormax, cmap = colormap)
        ax.yaxis.set_minor_locator(MultipleLocator(1))
        ax.xaxis.set_minor_locator(MultipleLocator(1))
        ax.yaxis.set_major_locator(MultipleLocator(2))
        ax.xaxis.set_major_locator(MultipleLocator(2))
        ax.set_xlim(xmin , xmax)
        ax.set_ylim(ymin,ymax)
        xlim = ax.get_xlim()
        ylim = ax.get_ylim()
        x_space = int(xlim[1]-xlim[0])//2
        if x_space == 0:
            x_space = 0.5
        xticks = np.arange(int(xlim[0]), xlim[1], x_space)
        y_space = int(ylim[1]-ylim[0])//2
        if y_space == 0:
            y_space = 0.5
        yticks = np.arange(int(ylim[0]), ylim[1], y_space)
        ax.set_xticks(xticks)
        ax.set_yticks(yticks)
        fig.subplots_adjust(hspace = 0, wspace = 0)
        if xmin is None:
            x_text = ax.get_xlim()[1] -0.5
        else:
            x_text = xmax - 0.5
        if ymax is None:
            y_text = ax.get_ylim()[1] -0.5
        else:
            y_text = ymax -0.5
        ax.text(x_text,y_text, 'v = {:.3f} km.s-1'.format(v_list[i]), color = textcolor, fontsize = speed_font)
        ax.grid()
    ax.set_xlim(ax.get_xlim()[::-1])
    cbar_ax = fig.add_axes([0.92, 0.15, 0.025, 0.7])
    fig.colorbar(im, cax=cbar_ax)
    fig.text(0.5,0.04, 'x (arcsec)', ha = 'center')
    fig.text(0.04,0.5, 'y (arcsec)', va = 'center', rotation = 'vertical')
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if show == True:
        plt.show()
    if close == True:
        plt.close()


#_________________________________________________________________________________
#_________________________________________________________________________________
# Reads VELO-LSR header for LSR relative speed
def read_v_lsr(filename):
    """
    ------------------------------------------------------
    ########################################################
    === Reads LSR relative speed from cube header ===
    ########################################################
    ### Input
        - filename : (str) cube name
    ### Output
        - v_lsr : (float) LSR relative speed stored in the 'VELO-LSR' header
    ### Notes

    ------------------------------------------------------
    """
    try:
        v_lsr = open_fits(filename)[0].header['VELO-LSR']
    except:
        print('####################')
        print('VELO-LSR keyword does not exist in this cube, will apply a v = 0 offset instead')
        print('####################')
        v_lsr = 0
    return v_lsr

#_________________________________________________________________________________
#_________________________________________________________________________________
# Returns the closest index to value in given array
def find_nearest(array,
                 value):
    """
    ------------------------------------------------------
    ########################################################
    === Finds the index of the nearest value in an array ===
    ########################################################
    ### Input
        - array : (list) list of elements
        - value : (float)
    ### Output
        - index of the nearest element of 'value' in 'array'
    ### Notes

    ------------------------------------------------------
    """
    array = np.asarray(array)
    idx = (np.abs(array - value)).argmin()
    return idx

#_________________________________________________________________________________
#_________________________________________________________________________________
# Returns index of smallest element higher than value
def find_smallest_superior(array,
                           value):
    """
    ------------------------------------------------------
    ########################################################
    === Finds the smallest index of an element of 'array' that is superior to 'value' ===
    ########################################################
    ### Input
        - array : (list)
        - value : (float)
    ### Output
        - i : (int) index
    ### Notes

    ------------------------------------------------------
    """
    for i in range(len(array)):
        if array[i] > value:
            break
    return i

#_________________________________________________________________________________
#_________________________________________________________________________________
# Returns index of highest element smaller than value

def find_highest_inferior(array,
                          value):
    """
    ------------------------------------------------------
    ########################################################
    === Finds the highest index of an element of 'array' that is inferior to 'value' ===
    ########################################################
    ### Input
        - array : (list)
        - value : (float)
    ### Output
        - i : (int) index
    ### Notes

    ------------------------------------------------------
    """
    size = len(array)
    array = array[::-1]
    for i in range(size):
        if array[i] < value:
            break
    return (size-1)-i


#_________________________________________________________________________________
#_________________________________________________________________________________
# Integrates the channel map and returns integrated array

def integrate_channelmap(filename = 'convolve.fits',
                         vmin = None,
                         vmax = None):
    """
    ------------------------------------------------------
    ########################################################
    === Returns the sum of all channels of a cube between 'vmin' and 'vmax' ===
    ########################################################
    ### Input
        - filename : (str) name of the cube
        - vmin : (float) minimum speed
        - vmax : (float) maximum speed
    ### Output
        - numpy 2D array, integrated channel map
    ### Notes
    Channels must be ordered by increasing speed
    ------------------------------------------------------
    """
    v_list = make_coord_list(filename, axis = 3)
    if vmin == None:
        vmin_idx = 0
    else:
        vmin_idx = find_smallest_superior(v_list, vmin)
        if vmin_idx !=0:
            vmin_idx = vmin_idx -1
    if vmax == None:
        vmax_idx = len(v_list)
    else:
        vmax_idx = find_highest_inferior(v_list, vmax)
        if vmax_idx != len(v_list):
            vmax_id = vmax_idx +1
    ar = fits_to_array(filename)[0]
    integ = np.zeros(np.shape(np.transpose(ar[0])))
    for i in range(vmin_idx,vmax_idx):
        integ = np.add(integ, np.transpose(ar[i]))
    return integ


#_________________________________________________________________________________
#_________________________________________________________________________________
# Finds the index of the minimum/maximum value in an array
def find_minmax(array,
                mode = 'min'):
    """
    ------------------------------------------------------
    ########################################################
    === Finds the min/max element in an array ===
    ########################################################
    ### Input
        - array : (list/numpy array)
        - mode : (str) 'min' or 'max', whether you wish to find the min or max element
    ### Output
        - (float) minimum/maximum  value
    ### Notes
    Works for an array of any shape
    ------------------------------------------------------
    """
    if mode == 'min':
        return np.unravel_index(array.argmin(), array.shape)
    if mode == 'max':
        return np.unravel_index(array.argmax(), array.shape)

#_________________________________________________________________________________
#_________________________________________________________________________________
# Convolves a channel map according to the given beam size
def convolve(filename,
             fwhm = 0.5,
             savename = 'convolve.fits'):
    """
    ------------------------------------------------------
    ########################################################
    === Convolves a cube to a beam size of 'fwhm' ===
    ########################################################
    ### Input
        - filename : (str) cube name
        - fwhm : (float) Full Width at Half Maximum of the convolving gaussian
        - savename : (str) name to the the convolved cube to
    ### Output
        - convolve.fits : convolved fits file
    ### Notes
    
    ------------------------------------------------------
    """
    lines = fits_to_array(filename)[0]
    file_nm = open_fits(filename)
    cdelt = abs(file_nm[0].header['CDELT1']) * 3600
    # LMH in pixels
    fwhm = fwhm/(cdelt)
    # Sigma in pixels
    sigma = fwhm /(2 * np.sqrt(2*np.log(2)))
    conv = gaussian_filter(lines, sigma = sigma)
    array_to_fits(savename, conv)
    print('cdelt', cdelt)
    print('fwhm', fwhm)
    print('sigma', sigma)
    copy_header(filename,savename)




#_________________________________________________________________________________
#_________________________________________________________________________________
# Plot the intensity of the integrated_over_channels channel map along a given axis : either x or y. Default is a slice at y = 0". Slice coordinate should be given in arcsec.
def pl_intensity(filename,
                 axis = 'y', 
                 cut = 0,
                 vmin = None,
                 vmax = None,
                 savename = None,
                 normalize = False, 
                 savefig = True, 
                 show = False, 
                 close = True,
                 font = 13):
    """
    ------------------------------------------------------
    ########################################################
    === Plots the intensity in a cube along a given axis at x = cut or y = cut ===
    ########################################################
    ### Input
        - filename : (str) cube name
        - axis : (str) 'x' or 'y', cut along this axis
        - cut : (float) cut position in the cube, in arcsec
        - savename : (str) savename for the cube. if None, will be saved to the format 'x_0.500_intensity_cut.png'
        - normalize : (bool) do you wish to normalzie the intensity to the maximum value
        - savefig : (bool) save the figure at the end
        - show : (bool) shows the figure at the end
        - close : (bool) closes the figure at the end
    ### Output
        - intensity_cut.png
    ### Notes
    
    ------------------------------------------------------
    """
    old_font = mpl.rcParams['font.size']
    mpl.rcParams['font.size'] = font
    
    for name in filename:
        lines = open_fits(name)
        hdr = lines[0].header

        ar = fits_to_array(name)[0]
        
        if axis == 'y':
            #tar = []
            #for i in ar:
                #tar.append(np.transpose(i))
            #ar = tar
            ax_val = 1
        elif axis != 'x':
            print('Axis must be either x or y')
            exit(0)
        else:
            ax_val = 0
        
        pos = coordinates(filename = name, make_cube = False)
        
        pos_list = pos[ax_val]
        if cut not in pos_list:
            slice_index = find_nearest(pos_list,cut)
            cut = pos_list[slice_index]
            print('This coordinate is not in the grid')
            print('Closest coordinate is : ', cut)
        else:
            slice_index = np.where(pos_list == cut)

        tot_emission = integrate_channelmap(filename = name, vmin = vmin, vmax = vmax)
        if axis == 'x':
            tot_emission = np.transpose(tot_emission)
        
        vals = []
        other_list = pos[abs(ax_val-1)]
        for i in range(len(other_list)):
            vals.append(tot_emission[i][slice_index])
        
        if normalize == True:
            vals = np.divide(vals, np.max(vals))
        
        plt.plot(other_list, vals, label = str(split_pathnm(name)[1]) + 'cut_at_' + axis + '_{:.3f}'.format(cut))
        if axis == 'x':
            x_axis = 'y'
        else:
            x_axis = 'x'
        plt.xlabel(x_axis + ' (arcsec)')
    
    plt.title('Integrated intensity')
    if savename is None:
        savename = (axis + '{:.3f}' + '_intensity_cut.png').format(cut)
    if savefig == True:
        if len(filename) == 1:
            plt.savefig(split_pathnm(filename[0])[0] + savename, bbox_inches = 'tight')
        else:
            plt.savefig(savename)
    if show == True:
        plt.show()
    if close == True:
        plt.close()
    mpl.rcParams['font.size'] = old_font



#_________________________________________________________________________________
#_________________________________________________________________________________
# Plots the spectrum at the given coordinate
def pl_spectrum(filename,
                savename = 'spectrum.png',
                loc = [0,0],
                vmin = None,
                vmax = None,
                ymin = None,
                ymax = None,
                normalize = False,
                v_offset = None,
                font = 13,
                savefig = True,
                show = False,
                close = True):
    """
    ------------------------------------------------------
    ########################################################
    === Plots the spectrum of a cube at the given location ===
    ########################################################
    ### Input
        - filename : (str) cube name
        - savename : (str) savename for the spectrum
        - loc : (2 elements list) [xpos, ypos], positions, in arcsec
        - vmin : (float) minimum speed on the spectrum
        - vmax : (float) maximum speed on the spectrum
        - ymin : (float) minimum value on the y axis
        - ymax : (float) maximum value on the y axis
        - normalize : (bool) do you wish to normalize the intensity to the maximum value
        - v_offset : (float) do you wish to apply a speed offset to the spectrum
        - font : (int) font size
        - savefig : (bool) saves th efigure at the end
        - show : (bool) shows the figure at the end
        - close : (bool) clsoes the figure at the end
    ### Output
        - spectrum.png
    ### Notes
    
    ------------------------------------------------------
    """
    old_font = mpl.rcParams['font.size']
    mpl.rcParams['font.size'] = font
    for name in filename:
        
        array = fits_to_array(name)[0]
        
        vlist = make_coord_list(filename = name, axis = 3)
        if v_offset is None:
            v_offset = 0
        vlist = np.add(vlist,v_offset)
        v_lsr = read_v_lsr(name)
        vlist = np.add(vlist,v_lsr)
        
        pos = coordinates(filename = name, make_cube = False)

        xindex = find_nearest(pos[0],loc[0])
        yindex = find_nearest(pos[1],loc[1])
        
        val = []
        for i in range(len(array)):
            val.append(array[i][yindex][xindex])
        
        maxi = np.max(val)
        if normalize == True:
            if maxi == 0:
                return 0
            val = np.divide(val, maxi)

        plt.plot(vlist,val, label = str(split_pathnm(name)[1]) + ' x = {:.3f}", y = {:.3f}"'.format(pos[0][xindex], pos[1][yindex]))
        plt.xlabel('Speed (km/s)')
        plt.legend()
        plt.xlim((vmin,vmax))
        plt.ylim((ymin,ymax))
        if normalize == True:
            plt.title('Normalized intensity')
        else:
            plt.title('Intensity/Flux')
    
    if savefig == True:
        if len(filename) == 1:
            plt.savefig((split_pathnm(filename[0])[0] + 'x{:.3f}_y{:.3f}_' + savename).format(pos[0][xindex], pos[1][yindex]), bbox_inches = 'tight')
        else:
            plt.savefig(('x{:.3f}_y{:.3f}_' + savename).format(pos[0][xindex], pos[1][yindex]), bbox_inches = 'tight')
    if show == True:
        plt.show()
    if close == True:
        plt.close()
    mpl.rcParams['font.size'] = old_font


#_________________________________________________________________________________
#_________________________________________________________________________________
# Turns Frequency header to speed header
def hdr_freq_to_speed(filename,
                      nu0 = 226.87478e9):
    """
    ------------------------------------------------------
    ########################################################
    === Turns a frequency header to a speed header, centered in nu0 ===
    ########################################################
    ### Input
        - filename : (str) cube name
        - nu0 : (float) reference frequency
    ### Output
        - cube.fits with changed header
    ### Notes
    
    ------------------------------------------------------
    """
    
    c = c_si / 1000
    filenew = os.path.splitext(filename)[0] + '_speedhdr' + os.path.splitext(filename)[1]
    shutil.copy(filename, filenew)
    lines = open_fits(filenew, 'update')
    hdr = lines[0].header
    if hdr['CTYPE3'] != 'VELO-LSR':
        hdr['CTYPE3'] = 'VELO-LSR'
        hdr['CRVAL3'] = -c * (hdr['CRVAL3']-nu0)/nu0
        hdr['CDELT3'] = -c * hdr['CDELT3']/nu0
    lines.close()
    add_header(filenew, 'NU0', nu0)


def draw_dutrey_contours(marker = '+', 
                         savename = 'dutrey_h2_density_contours.png', 
                         savefig = False, 
                         close = False, 
                         legend = False, 
                         show = False, 
                         directory = '/user/homedir/demarsd/Documents/stage/data/dutrey/'):
    names = ['h2_density_1e5.txt','h2_density_1e6.txt','h2_density_1e7.txt','h2_density_1e8.txt']
    for name in names:
        [r,z] = np.loadtxt(directory + name, delimiter = ";", unpack=True, usecols=[0,1])
        plt.scatter(r,z, marker = marker, label = name)
    if legend == True:
        plt.legend()
    if savefig == True:
        plt.savefig(savename, bbox_inches = 'tight')
    if show == True:
        plt.show()
    if close == True:
        plt.close()

def split_pathnm(filename):
    splited = os.path.split(filename)
    if splited[0] == '':
        fullpathnm = os.getcwd() + '/'
    else:
        cwd = os.getcwd()
        os.chdir(splited[0])
        fullpathnm = os.getcwd() + '/'
        os.chdir(cwd)
    filenm = splited[1]
    full_filenm = fullpathnm + filenm
    return [fullpathnm, filenm, full_filenm]
