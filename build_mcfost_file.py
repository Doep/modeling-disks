#!/user/homedir/demarsd/.nix-profile/bin/python

"""

########################################
##### This is build_mcfost_file.py #####
########################################

This routine will create the MCFOST parameter file -- later used to run MCFOST -- by using parameters given with -filename

##################################

"""


import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-f', '--filename', help = 'filename from which to read parameters', type = str, required = True)
parser.add_argument('-s', '--savename', nargs ='?', default = 'mcfost_parameters.para', help = 'savename for the mcfost parameters file', type = str)

args = parser.parse_args()


filename = args.filename
savename = args.savename


from routines import make_mcfost_para_file


make_mcfost_para_file(filename = filename, savename = savename)
