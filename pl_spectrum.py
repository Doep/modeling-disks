#!/user/homedir/demarsd/.nix-profile/bin/python

"""

##################################
##### This is pl_spectrum.py #####
##################################

This routine will draw the spectrum on a specified location in a cube.
The location must be given with : -p xpos ypos

-> Note that it is possible to plot the spectrum in a given location for multiple cubes at once. Simply give all the cube you want to plot the spectrum from, with : $ pl_spectrum.py -f cube1.fits cube2.fits [--other_parameters]

##################################

"""


import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)
parser.add_argument('-f', '--filename', nargs = '+', help = 'fits cube to plot the spectrum from', type = str, required = True)
parser.add_argument('-p', '--pos', nargs=2, help = 'position in arcsec, as : -p x y', type = float, required = True)
parser.add_argument('-s', '--savename', nargs ='?', default = 'spectrum.png', help = 'savename for the graph', type = str)
parser.add_argument('-xmin', help = 'x minimum value on the graph; if you set xmin, you must also set xmax', type =float)
parser.add_argument('-xmax', help = 'x maximum value on the graph; if you set xmax, you must also set xmin', type =float)
parser.add_argument('-ymin', help = 'y minimum value on the graph; if you set ymin, you must also set ymax', type =float)
parser.add_argument('-ymax', help = 'y maximum value on the graph; if you set ymax, you must also set ymin', type =float)
parser.add_argument('-n', '--normalize', help = 'choose to normalize or not the spectrum to the maximum value. default is True', action = 'store_false')
parser.add_argument('-v', '--v_offset', help = 'speed offset on the spectrum', type = float)
parser.add_argument('-fs', '--figsize', nargs = 2, default = (10,10), help = 'size of the figure, pass as : -fs xsize ysize', type = float)
parser.add_argument('-font', nargs = '?', default = 13, help = 'font size', type = int)
parser.add_argument('-sh', '--show', help = 'show the spectrum after execution', action = 'store_false')

args = parser.parse_args()


filename = args.filename
pos = args.pos
xmin = args.xmin
xmax = args.xmax
ymin = args.ymin
ymax = args.ymax
normalize = args.normalize
v_offset = args.v_offset
savename = args.savename
figsize = args.figsize
font = args.font
show = args.show


from routines import pl_spectrum
import matplotlib.pyplot as plt


plt.figure(figsize = figsize)
pl_spectrum(filename, loc = pos, vmin = xmin, vmax = xmax, ymin = ymin, ymax = ymax, normalize = normalize, v_offset = v_offset, savename = savename, font = font, show = show)
