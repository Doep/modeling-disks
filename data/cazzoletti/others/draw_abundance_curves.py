from routines import *
import numpy as np
import matplotlib.pyplot as plt


#curveupname = 'curve_up_1e-08.txt'
#curvedownname = 'curve_down_1e-08.txt'


#rup,zup = np.loadtxt(curveupname, delimiter = ';', unpack = True, usecols = [0,1])
#rdown,zdown = np.loadtxt(curvedownname, delimiter = ';', unpack = True, usecols = [0,1])

#zup = rup*zup
#zdown = rdown*zdown

#plt.scatter(rup,zup/rup, marker = '+')
#plt.scatter(rdown,zdown/rdown, marker = '+')
#plt.xlim((0,600))
#plt.show()



filename = 'normalized_abundance.txt'

r,z = np.loadtxt(filename, delimiter = ';', unpack = True, usecols = [0,1])

plt.plot(r,z)
plt.xlabel('R (au)')
plt.ylabel('normalized abundance')
plt.savefig('normalized_abundance.png')
