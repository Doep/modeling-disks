


from routines import *
import numpy as np
from scipy.interpolate import interp1d

#p = -0.5
sigma0 = 1e23
r0 = 100
rc = 50
h0 = 11.3
#h = -1.3

#gamma = 2


def sigma(r,sigma0, r0, p , gamma, rc):
    val = sigma0*(r/r0)**(-p) * np.exp(-(r/rc)**(2-gamma))
    return val

def scale_height(r, r0, h):
    val = h0*(r/r0)**(-h)
    return val

def z_val(dens, r, sigma0, r0, p, gamma, rc, h):
    sheight = scale_height(r = r, r0 = r0, h = h)
    sigval = sigma(r = r, sigma0 = sigma0, r0 = r0, p = p, gamma = gamma, rc = rc)
    val =  sheight**2 * np.log(sigval/(dens*sheight*au_cgs*np.sqrt(np.pi)))
    if val < 0:
        return np.inf
    else:
        return np.sqrt(val)
    return val

#[r_dutrey,z_dutrey] = np.loadtxt('h2_density_1e8.txt', delimiter = ";", unpack=True, usecols=[0,1])


names = ['h2_density_1e5.txt','h2_density_1e6.txt','h2_density_1e7.txt','h2_density_1e8.txt']
density = [1e5,1e6,1e7,1e8]
#density = np.dot(density,0.1)
#sigma0 = 0.1 * sigma0
interps = []
rmin = []
rmax = []
for name in names:
    [r_dutrey,z_dutrey] = np.loadtxt(name, delimiter = ";", unpack=True, usecols=[0,1])
    rmin.append(np.min(r_dutrey))
    rmax.append(np.max(r_dutrey))
    interps.append(interp1d(r_dutrey,z_dutrey, kind = 'linear'))

#________________________________
p_list = np.linspace(-3,3,2)
p_list = [1.28]
h_list = np.linspace(-2,0,2)
h_list = [-1.292]
gamma_list = np.linspace(-2,4,801)
gamma_list = [3.911]
#sig0 = np.dot([1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20],1e22)
para = [p_list,h_list,gamma_list]
#________________________________________


error = []
pmax = np.max(p_list)
hmax = np.max(h_list)

for p in p_list:
    sum_p = []
    for h in h_list:
        sum_h = []
        print('p : ' +str(p) + '/' + str(pmax) + ' !!! h : ' +str(h) + '/' + str(hmax))
        for gamma in gamma_list:
            sum_gamma = 0
            nbr = 0
            for i in range(len(names)):
                dens = density[i]
                name = names[i]
                interp = interps[i]
                r_min = rmin[i]
                r_max = rmax[i]
                r_list = np.linspace(r_min,r_max,30)
                for r in r_list:
                    z_mod = z_val(dens = dens, r = r, sigma0 = sigma0, r0 = r0, p = p, gamma = gamma, rc = rc, h = h)
                    if np.isinf(z_mod):
                        #print('ok')
                        #print(p, h, gamma, name, r)
                        continue
                        #sum_gamma = 1e20
                        #flag = True
                        #break
                    else:
                        z = interp(r)
                        loc_error2 = (z_mod - z)**2
                        sum_gamma += loc_error2
                        nbr += 1
            if nbr <= 10:
                sum_gamma = 1e22
                sum_h.append([sum_gamma,sum_gamma,nbr])
            else:
                sum_h.append([sum_gamma, sum_gamma/nbr, nbr])
        sum_p.append(sum_h)
    error.append(sum_p)

error = np.array(error)

savename = 'p_{:.3f}_{:.3f}_h_{:.3f}_{:.3f}_gamma_{:.3f}_{:.3f}.fits'.format(np.min(p_list), np.max(p_list), np.min(h_list), np.max(h_list), np.min(gamma_list), np.max(gamma_list))


try:
    array_to_fits(savename,error, subs = para)
except:
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('------- CUBE SAVING FAILED -------')
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!')

chi2list = np.transpose(np.transpose(error)[0][:][:][:])
chi2redlist = np.transpose(np.transpose(error)[1][:][:][:])
#chi2crosslist = np.transpose(np.transpose(error)[2][:][:][:])

indices = np.unravel_index(chi2list.argmin(), chi2list.shape)
indicesred = np.unravel_index(chi2redlist.argmin(), chi2redlist.shape)
#indicescross = np.unravel_index(chi2crosslist.argmin(), chi2crosslist.shape)
print('###############################')
print('Indices')
print(indices)
print('###############################')
print('Best chi2 fit :')
print('p = {:.3f}'.format(p_list[indices[0]]))
print('h = {:.3f}'.format(h_list[indices[1]]))
print('gamma = {:.3f}'.format(gamma_list[indices[2]]))
print('chi2 = {:.3f}'.format(chi2list[indices]))
print('nbr = {:.1f}'.format(error[indices[0]][indices[1]][indices[2]][2]))
print('###############################')
print('Best chi2red fit :')
print('p = {:.3f}'.format(p_list[indicesred[0]]))
print('h = {:.3f}'.format(h_list[indicesred[1]]))
print('gamma = {:.3f}'.format(gamma_list[indicesred[2]]))
print('chi2red = {:.3f}'.format(chi2redlist[indicesred]))
print('nbr = {:.1f}'.format(error[indicesred[0]][indicesred[1]][indicesred[2]][2]))
print('###############################')
#print('Best chi2cross fit :')
#print('p = {:.3f}'.format(p_list[indicescross[0]]))
#print('h = {:.3f}'.format(h_list[indicescross[1]]))
#print('gamma = {:.3f}'.format(gamma_list[indicescross[2]]))
#print('chi2cross = {:.3f}'.format(chi2crosslist[indicescross]))
#print('nbr = {:.1f}'.format(error[indicescross[0]][indicescross[1]][indicescross[2]][3]))

print('###############################')
print('Other decent fits')
chi2_max = 25
i = 0
for p in range(len(error)):
    for h in range(len(error[p])):
        for gamma in range(len(error[p][h])):
            if error[p][h][gamma][0] <= chi2_max:
                print('---------------------------------')
                print('p = {:.3f}'.format(p_list[p]))
                print('h = {:.3f}'.format(h_list[h]))
                print('gamma = {:.3f}'.format(gamma_list[gamma]))
                print('chi2 = {:.3f}'.format(error[p][h][gamma][0]))
                print('nbr = {:.1f}'.format(error[p][h][gamma][2]))
                i+=1

print('###############################')
print('number of decent fits : ' +str(i))


#error = error.flatten()
#plt.hist(error, bins = 100)
#plt.show()

