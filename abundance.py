#!/user/homedir/demarsd/.nix-profile/bin/python

"""
################################
##### This is abundance.py #####
################################

This routine will :
    - Run : $ mcfost filename -disk_struct
    - Read generated grid.fits and cazzoletti files to create abundance.fits
    - Run : $ mcfost filename -mol
    - Separate lines.fits into all lines components with continuum subtracted
    - Create diff.fits files for each ray, representing the sign of the emission on the channel maps
    - Sum all lines by setting reference speed v=0 for the first ray
    - Convolve the channel map to a beam size of 0.5"
    - Compute CN and H2 number density into CN_density.fits and gas_density_true.fits

 -> Note that you can skip the generation of the abundance.fits file by giving your own abundance.fits file with the -abfile parameter

##################################

"""


import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('-f', '--filename', help = 'MCFOST parameter file name', type = str, required = True)
parser.add_argument('-d', '--default', help = 'default parameters filename', type = str, required = True)
parser.add_argument('-o', '--output_dir', help = 'ouput files directory, if none given, outputs/ will be created', type = str)
parser.add_argument('-r', '--resize', help = 'manual resizing factor for the distribution; if not given, it will automatically scale he ditribution the disk size', type = float)
parser.add_argument('-s', '--sigma_resize', help = 'manual resizing factor for the sigma of the distribution', type = float)
parser.add_argument('-z', '--zexpand', help = 'manual resizing factor for the height of the distribution', type = float)
parser.add_argument('-a', '--abmax_factor', help = 'factor applied the the abundance distribution', type = float)
#parser.add_argument('-dar', '--dary_scale_height', help = 'uses dary scale heights as the location of maximum of abundance at each radius', action = 'store_true')
parser.add_argument('-t', '--temp_rescale', help = 'temperature at which to rescale the distribution', type = float)
parser.add_argument('-rad', '--radius_temp',nargs='?', default = 100, help = 'radius at which to rescale the distribution', type = float)
parser.add_argument('-abfile', '--abfile', help = 'abundance file if you do not wish to make it built by this program', type = str)

args = parser.parse_args()


filename = args.filename
default = args.default
output_dir = args.output_dir
resize = args.resize
sigma_resize = args.sigma_resize
zexpand = args.zexpand
abmax_factor = args.abmax_factor
#dary_mode = args.dary_scale_height
temp_rescale = args.temp_rescale
radius_temp = args.radius_temp
abfile = args.abfile


import os
from routines import *
import subprocess
import shutil


if abfile is not None:
    abfile = split_pathnm(abfile)[2]
# Add PATH to default filename
default = split_pathnm(default)[2]

# Split filename and path
splited = split_pathnm(filename)
input_dir = splited[0]
filename = splited[2]

if output_dir != None:
    try:
        os.chdir(output_dir)
        output_dir = os.getcwd() + '/'
        os.chdir(input_dir)
    except:
        print('Output directory does not exist yet. Please create it before running this code.')
        exit(0)
else:
    os.chdir(input_dir)
    flag = True
    i = 0
    name_output = 'outputs'
    new_name = name_output
    while flag == True:
        try:
            os.mkdir(new_name)
            flag = False
            output_dir = os.getcwd() + '/' + new_name + '/'
        except:
            new_name = name_output + str(i)
            i = i+1

# Loads essential parameters
para = read_default_parameters(filename = default)
v_lsr = para['v_lsr']

print('_______________________________________________')
print('_______________________________________________')
print('MCFOST -disk_struct, computing grid')

# Create grid.fits
working_dir = 'working_dir'
flag = True
i = 0
while flag == True:
    try:
        os.mkdir(working_dir)
        flag = False
        working_dir = os.getcwd() + '/' + working_dir + '/'
    except:
        working_dir = working_dir + str(i)
        i = i+1
        
shutil.copy(filename, working_dir)
os.chdir(working_dir)
subprocess.call('mcfost ' + filename + ' -disk_struct', shell = True)
subprocess.call('gunzip data_disk/grid.fits', shell = True)


print('_______________________________________________')
print('_______________________________________________')
print('Creating abundance.fits')

# Create abundance.fits
if abfile is None:
    out_ab = abundance_cazz(directory = working_dir, default_filename = default, resize = resize, sigma_resize = sigma_resize, zexpand = zexpand, abmax_factor = abmax_factor, reduce_at_temp = temp_rescale, reduce_radius = radius_temp)
else:
    shutil.copy(abfile, working_dir + 'abundance.fits')

print('_______________________________________________')
print('_______________________________________________')
print('MCFOST -mol, computing lines')
# Create lines.fits
subprocess.call('mcfost ' + filename + ' -mol', shell = True)

subprocess.call('mv ' + working_dir + 'data_*/* ' + output_dir + ' -f', shell = True)
shutil.copy(working_dir + 'abundance.fits', output_dir)
shutil.rmtree(working_dir,ignore_errors=True)

os.chdir(output_dir)

# Add VELO-LSR header to lines.fits
subprocess.call('gunzip lines.fits.gz', shell = True)
add_header('lines.fits', 'VELO-LSR', v_lsr, comment = 'LSR relative speed in km/s')

# Compute CN density
print('Computing CN density')

subprocess.call('gunzip gas_density.fits.gz', shell = True)

compute_density()

# Removes continuum from lines.fits and separates the rays
print('Removing continuum, separating rays, summing rays based on v=0 centered on ray0')
continuum_removal()

# Summing rays
sum_rays()

# Convolving by a beam of 0.5''
lmh = 0.5
print('Convolving by a beam of ' + str(lmh) +'"')
convolve('sum_hyperfine.fits', fwhm = lmh, savename = 'convolve.fits')

subprocess.call('gzip *.fits', shell = True)

if out_ab == 1:
    print('!!!!!!!!!!!!!!!!!!!!!!!!!!')
    print('REMINDER : Abundance was not rescaled to asked radius at ask temperature as this temperature does not exit at this radius.')









