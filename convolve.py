#!/user/homedir/demarsd/.nix-profile/bin/python

import argparse


parser = argparse.ArgumentParser(description = __doc__, formatter_class=argparse.RawDescriptionHelpFormatter)

parser.add_argument('-f', '--filename', help = 'fits file to convolve', type = str, required = True)
parser.add_argument('-fwhm', '--fwhm', help = 'fwhm of the beam to convolve with', type = float, required = True)
parser.add_argument('-s', '--savename', nargs = '?', default = 'convolve.fits', help = 'name to save the convolved channel map to', type = str)

args = parser.parse_args()

filename = args.filename
fwhm = args.fwhm
savename = args.savename

from routines import convolve

convolve(filename = filename, fwhm = fwhm, savename = savename)
